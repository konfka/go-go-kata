package main

import "testing"

var (
	pets Pets
	err  error
	data []byte
)

func BenchmarkStandardJson(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(data)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkJsonIterUnmarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(data)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
	}
}
