package main

import (
	"fmt"
)

type calc struct {
	a, b   float64
	result interface{}
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c calc) SetA(a float64) *calc {
	c.a = a
	return &c
}

func (c calc) SetB(b float64) *calc {
	c.b = b

	return &c
}

func (c calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)

	return &c
}

func (c *calc) Result() interface{} {
	return (*c).result
}

func multiply(a, b float64) float64 {
	return a * b
}

func divide(a, b float64) interface{} {
	if b == 0 {
		return "Division by zero error!"
	}
	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return (a + b) / 2
}

func main() {
	calc := NewCalc()
	res := calc.SetA(10).SetB(34).Do(multiply).Result()
	resPointer := &res
	fmt.Println(res)
	var res2 = *resPointer
	fmt.Println(res2)
	if res != res2 {
		panic("object statement is not persist")
	}
}
