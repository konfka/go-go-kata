package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func BenchmarkMultiply(b *testing.B) {
	var test = []struct {
		name string
		args calc
		want float64
	}{
		{
			name: "1 * 0",
			args: calc{
				a: 1,
				b: 0},
			want: 0},
		{
			name: "0 * 1",
			args: calc{
				a: 0,
				b: 1},
			want: 0},
		{
			name: "111 * 111",
			args: calc{
				a: 111,
				b: 111},
			want: 111 * 111},
		{
			name: "12 * 1",
			args: calc{
				a: 12,
				b: 1},
			want: 12},
		{
			name: "800 * 12",
			args: calc{
				a: 800,
				b: 12},
			want: 800 * 12},
		{
			name: "10000 * 10000",
			args: calc{
				a: 10000,
				b: 10000},
			want: 100000000},
		{
			name: "12 * 15",
			args: calc{
				a: 12,
				b: 15},
			want: 12 * 15},
		{
			name: "7 * 7",
			args: calc{
				a: 7,
				b: 7},
			want: 7 * 7},
	}
	for _, bb := range test {
		b.Run(bb.name, func(b *testing.B) {
			assert.Equalf(b, bb.want, multiply(bb.args.a, bb.args.b), "Multiply(%v, %v)", bb.args.a, bb.args.b)
		})
	}
}

func BenchmarkDivide(b *testing.B) {
	test := []struct {
		name string
		args calc
	}{
		{
			name: "1 / 0",
			args: calc{
				a:      1,
				b:      0,
				result: "Division by zero error!",
			},
		},
		{
			name: "12567 / 1543",
			args: calc{
				a:      12567,
				b:      1543,
				result: float64(12567) / float64(1543)},
		},
		{
			name: "0 / 1",
			args: calc{
				a:      0,
				b:      1,
				result: float64(0) / float64(1)},
		},
		{
			name: "111 / 111",
			args: calc{
				a:      111,
				b:      111,
				result: float64(111) / float64(111)},
		},
		{
			name: "12 / 1",
			args: calc{
				a:      12,
				b:      1,
				result: float64(12) / float64(1)},
		},
		{
			name: "800 / 12",
			args: calc{
				a:      800,
				b:      12,
				result: float64(800) / float64(12)},
		},
		{
			name: "1000 / 10000",
			args: calc{
				a:      1000,
				b:      10000,
				result: float64(1000) / float64(10000)},
		},
		{
			name: "12 / 15",
			args: calc{
				a:      12,
				b:      15,
				result: float64(12) / float64(15)},
		},
		{
			name: "18 / 7",
			args: calc{
				a:      18,
				b:      7,
				result: float64(18) / float64(7)},
		},
	}
	for _, bb := range test {
		b.Run(bb.name, func(b *testing.B) {
			assert.Equalf(b, bb.args.result, divide(bb.args.a, bb.args.b), "Divide(%v, %v)", bb.args.a, bb.args.b)
		})
	}
}

func BenchmarkSum(b *testing.B) {
	test := []struct {
		name string
		args calc
	}{
		{
			name: "12567 + 1543",
			args: calc{
				a:      12567,
				b:      1543,
				result: float64(12567) + float64(1543)},
		},
		{
			name: "0 + 1",
			args: calc{
				a:      0,
				b:      1,
				result: float64(0) + float64(1)},
		},
		{
			name: "111 + 111",
			args: calc{
				a:      111,
				b:      111,
				result: float64(111) + float64(111)},
		},
		{
			name: "12 + 1",
			args: calc{
				a:      12,
				b:      1,
				result: float64(12) + float64(1)},
		},
		{
			name: "800 + 12",
			args: calc{
				a:      800,
				b:      12,
				result: float64(800) + float64(12)},
		},
		{
			name: "1000 + 10000",
			args: calc{
				a:      1000,
				b:      10000,
				result: float64(1000) + float64(10000)},
		},
		{
			name: "12 + 15",
			args: calc{
				a:      12,
				b:      15,
				result: float64(12) + float64(15)},
		},
		{
			name: "18 + 7",
			args: calc{
				a:      18,
				b:      7,
				result: float64(18) + float64(7)},
		},
	}
	for _, bb := range test {
		b.Run(bb.name, func(b *testing.B) {
			assert.Equalf(b, bb.args.result, sum(bb.args.a, bb.args.b), "Sum(%v, %v)", bb.args.a, bb.args.b)
		})
	}
}

func BenchmarkAverage(b *testing.B) {
	test := []struct {
		name string
		args calc
	}{
		{
			name: "(12567 + 1543) / 2",
			args: calc{
				a:      12567,
				b:      1543,
				result: (float64(12567) + float64(1543)) / 2},
		},
		{
			name: "(0 + 1) / 2",
			args: calc{
				a:      0,
				b:      1,
				result: (float64(0) + float64(1)) / 2},
		},
		{
			name: "(111 + 111) / 2",
			args: calc{
				a:      111,
				b:      111,
				result: (float64(111) + float64(111)) / 2},
		},
		{
			name: "(12 + 1) / 2",
			args: calc{
				a:      12,
				b:      1,
				result: (float64(12) + float64(1)) / 2},
		},
		{
			name: "(800 + 12) / 2",
			args: calc{
				a:      800,
				b:      12,
				result: (float64(800) + float64(12)) / 2},
		},
		{
			name: "(1000 + 10000) / 2",
			args: calc{
				a:      1000,
				b:      10000,
				result: (float64(1000) + float64(10000)) / 2},
		},
		{
			name: "(12 + 15) / 2",
			args: calc{
				a:      12,
				b:      15,
				result: (float64(12) + float64(15)) / 2},
		},
		{
			name: "(18 + 7) / 2",
			args: calc{
				a:      18,
				b:      7,
				result: (float64(18) + float64(7)) / 2},
		},
	}
	for _, bb := range test {
		b.Run(bb.name, func(b *testing.B) {
			assert.Equalf(b, bb.args.result, average(bb.args.a, bb.args.b), "Sum(%v, %v)", bb.args.a, bb.args.b)
		})
	}
}
