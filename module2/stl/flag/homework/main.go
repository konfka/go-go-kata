package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"AppName"`
	Production bool   `json:"Production"`
}

func check(e error) {
	if e != nil {
		fmt.Println(e)
		os.Exit(0)
	}
}

func main() {
	text := []byte(`{"AppName":"King Royal", "Production":true}`)
	isValid := json.Valid(text)
	fmt.Println(isValid)
	var conf Config
	err := json.Unmarshal(text, &conf)
	check(err)
	fmt.Printf("%#v\n", conf)
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
