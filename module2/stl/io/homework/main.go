package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	str := new(bytes.Buffer)
	// запишите данные в буфер
	for i := range data {
		str.WriteString(data[i] + "\n")
	}
	// создайте файл
	file, err := os.Create("./example.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	defer file.Close()
	// запишите данные в файл
	if _, err := io.Copy(file, str); err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	fmt.Println(file)
	// прочтите данные в новый буфер
	var str2 bytes.Buffer
	f, err := os.Open("./example.txt")
	defer func() {
		if cerr := f.Close(); cerr != nil && err == nil {
			err = cerr
		}
	}()
	defer func() {
		if err != nil {
			fmt.Println(err)
			os.Exit(0)
		}
	}()
	_, err = str2.ReadFrom(f)
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	// выведите данные из буфера buffer.String()
	fmt.Println(str2.String())
	// у вас все получится!
}
