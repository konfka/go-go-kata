package main

import "fmt"

func generateSelfHistory(name string, age int, money float64) string {
	return fmt.Sprintf("Hello! My name is %v. I'm %d y.o. And also have $%f in my wallet right now.", name, age, money)
}

func main() {
	fmt.Println(generateSelfHistory("Marat", 24, 99124500))
}
