package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	f, err := os.Create("/tmp/dat2")
	check(err)

	n3, err := f.WriteString(name)
	check(err)
	fmt.Printf("wrote %d bytes\n", n3)

}
