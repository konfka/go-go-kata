package main

import (
	"os"
	"strings"
)

var RuTransiltToEng = map[rune]string{
	'а': "a",
	'б': "b",
	'в': "v",
	'г': "g",
	'д': "d",
	'е': "e",
	'ё': "yo",
	'ж': "zh",
	'з': "z",
	'и': "i",
	'й': "j",
	'к': "k",
	'л': "l",
	'м': "m",
	'н': "n",
	'о': "o",
	'п': "p",
	'р': "r",
	'с': "s",
	'т': "t",
	'у': "u",
	'ф': "f",
	'х': "h",
	'ц': "c",
	'ч': "ch",
	'ш': "sh",
	'щ': "sch",
	'ъ': "'",
	'ы': "y",
	'ь': "",
	'э': "e",
	'ю': "ju",
	'я': "ja",
	'А': "A",
	'Б': "B",
	'В': "V",
	'Г': "G",
	'Д': "D",
	'Е': "E",
	'Ё': "Yo",
	'Ж': "Zh",
	'З': "Z",
	'И': "I",
	'Й': "J",
	'К': "K",
	'Л': "L",
	'М': "M",
	'Н': "N",
	'О': "O",
	'П': "P",
	'Р': "R",
	'С': "S",
	'Т': "T",
	'У': "U",
	'Ф': "F",
	'Х': "H",
	'Ц': "C",
	'Ч': "Ch",
	'Ш': "Sh",
	'Щ': "Sch",
	'Ы': "Y",
	'Э': "E",
	'Ю': "Ju",
	'Я': "Ja",
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	fIn, err := os.ReadFile("./example.txt")
	check(err)
	s := string(fIn)
	fOut, err := os.Create("./example.processed.txt")
	check(err)
	for cyrillic, latin := range RuTransiltToEng {
		s = strings.ReplaceAll(s, string(cyrillic), latin)
	}
	n3, err := fOut.WriteString(s)
	_ = n3
	check(err)
}
