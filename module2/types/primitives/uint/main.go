package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeUint()
}

func typeUint() {
	fmt.Println("=== START type uint ===")
	var NumberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", NumberUint8, "size is:", unsafe.Sizeof(NumberUint8), "bytes")
	NumberUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", NumberUint8, "size is:", unsafe.Sizeof(NumberUint8), "bytes")
	var NumberUint16 uint16 = (1 << 16) - 1
	var NumberUint32 uint32 = (1 << 32) - 1
	var NumberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint16 max value:", NumberUint16, "size is:", unsafe.Sizeof(NumberUint16), "bytes")
	fmt.Println("uint32 max value:", NumberUint32, "size is:", unsafe.Sizeof(NumberUint32), "bytes")
	fmt.Println("uint64 max value:", NumberUint64, "size is:", unsafe.Sizeof(NumberUint64), "bytes")
	fmt.Println("=== END type uint ===")
}
