package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var uintNumber uint8 = 1 << 7
	var from = int8(uintNumber)
	uintNumber--
	var to = int8(uintNumber)
	fmt.Println(from, to)
	typeUint()
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var NumberUint8 uint8 = 1 << 7
	var NumberUint16 uint16 = 1 << 15
	var NumberUint32 uint32 = 1 << 31
	var NumberUint64 uint64 = 1 << 63

	var MinNumberInt8 = int8(NumberUint8)
	NumberUint8--
	var MaxNumberInt8 = int8(NumberUint8)

	var MinNumberInt16 = int16(NumberUint16)
	NumberUint16--
	var MaxNumberInt16 = int16(NumberUint16)

	var MinNumberInt32 = int32(NumberUint32)
	NumberUint32--
	var MaxNumberInt32 = int32(NumberUint32)

	var MinNumberInt64 = int64(NumberUint64)
	NumberUint64--
	var MaxNumberInt64 = int64(NumberUint64)
	fmt.Println("int8 min value:", MinNumberInt8, "int8 max value:", MaxNumberInt8, "size is:", unsafe.Sizeof(MinNumberInt8), "bytes")
	fmt.Println("int16 min value:", MinNumberInt16, "int16 max value:", MaxNumberInt16, "size is:", unsafe.Sizeof(MinNumberInt16), "bytes")
	fmt.Println("int32 min value:", MinNumberInt32, "int32 max value:", MaxNumberInt32, "size is:", unsafe.Sizeof(MinNumberInt32), "bytes")
	fmt.Println("int64 min value:", MinNumberInt64, "int64 max value:", MaxNumberInt64, "size is:", unsafe.Sizeof(MinNumberInt64), "bytes")
	fmt.Println("=== END type int ===")
}
func typeUint() {
	fmt.Println("=== START type uint ===")
	var NumberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", NumberUint8, "size is:", unsafe.Sizeof(NumberUint8), "bytes")
	NumberUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", NumberUint8, "size is:", unsafe.Sizeof(NumberUint8), "bytes")
	var NumberUint16 uint16 = (1 << 16) - 1
	var NumberUint32 uint32 = (1 << 32) - 1
	var NumberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint16 max value:", NumberUint16, "size is:", unsafe.Sizeof(NumberUint16), "bytes")
	fmt.Println("uint32 max value:", NumberUint32, "size is:", unsafe.Sizeof(NumberUint32), "bytes")
	fmt.Println("uint64 max value:", NumberUint64, "size is:", unsafe.Sizeof(NumberUint64), "bytes")
	fmt.Println("=== END type uint ===")
}
