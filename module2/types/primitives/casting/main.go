package main

import "fmt"

func main() {
	var numberInt int = 3
	var floatNumber float32 = float32(numberInt)
	fmt.Printf("тип: %T, значение: %v", floatNumber, floatNumber)
}
