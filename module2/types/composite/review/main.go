package main

import (
	"fmt"
	"math/rand"
	"sort"
)

func init() {
}

type User struct {
	Name   string
	Age    int
	Income int
} // User создай структуру User.

// generateAge сгенерируй возраст от 18 до 70 лет.
func generateAge() int {
	i := rand.Intn((70 - 18 + 1) + 18)
	return i
}

// generateIncome сгенерируй доход от 0 до 500000.
func generateIncome() int {
	i := rand.Intn(500001)
	return i
}

// generateFullName сгенерируй полное имя. например "John Doe".
func generateFullName() string {
	names := []string{"Donald", "Joe", "Barak", "Dzinpin", "Way", "John", "Bob", "Richard"}
	surnames := []string{"Biden", "Trump", "Obama", "Xi", "Lao", "Johns", "Aboba", "Nice"}
	// создай слайс с именами и слайс с фамилиями.
	n := rand.Intn(len(names))
	s := rand.Intn(len(surnames))
	return names[n] + " " + surnames[s]
}

func main() {
	users := make([]User, 1000)
	for i := 0; i < 1000; i++ {
		users[i] = User{
			Name:   generateFullName(),
			Age:    generateAge(),
			Income: generateIncome(),
		}
	}
	// Сгенерируй 1000 пользователей и заполни ими слайс users.

	var midAge int
	for i := range users {
		midAge += users[i].Age
	}
	midAge /= len(users)

	fmt.Println(midAge)
	// Выведи средний возраст пользователей.
	var midIncome float64
	for i := range users {
		midIncome += float64(users[i].Income)
	}
	midIncome /= float64(len(users))

	fmt.Println(midIncome)
	// Выведи средний доход пользователей.
	var count int
	for i := range users {
		if float64(users[i].Income) > midIncome {
			count++
		}
	}
	fmt.Println(count)
	// Выведи количество пользователей, чей доход превышает средний доход.
	sort.Slice(users, func(i, j int) bool {
		return users[i].Age < users[j].Age
	})
	fmt.Println(users)
}
