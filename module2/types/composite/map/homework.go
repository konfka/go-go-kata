package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/nsarrazin/serge",
			Stars: 1811,
		},
		{
			Name:  "https://github.com/madawei2699/myGPTReader",
			Stars: 2016,
		},
		{
			Name:  "https://github.com/mrsked/mrsk",
			Stars: 4499,
		},
		{
			Name:  "https://github.com/programthink/zhao",
			Stars: 2687,
		},
		{
			Name:  "https://github.com/BlinkDL/RWKV-LM",
			Stars: 3695,
		},
		{
			Name:  "https://github.com/mckaywrigley/chatbot-ui",
			Stars: 3673,
		},
		{
			Name:  "https://github.com/LC1332/Chinese-alpaca-lora",
			Stars: 581,
		},
		{
			Name:  "https://github.com/dragonflydb/dragonfly",
			Stars: 17949,
		},
		{
			Name:  "https://github.com/LianjiaTech/BELLE",
			Stars: 1834,
		},
		{
			Name:  "https://github.com/codebdy/rxdrag",
			Stars: 2247,
		},
		{
			Name:  "https://github.com/Leizhenpeng/feishu-chatgpt",
			Stars: 1910,
		},
		{
			Name:  "https://github.com/fauxpilot/fauxpilot",
			Stars: 9768,
		},
	}
	mapp := make(map[string]Project)

	for i := range projects {
		mapp[projects[i].Name] = projects[i]
	}

	for k, v := range mapp {
		fmt.Println(k, "-", v)
	}
}
