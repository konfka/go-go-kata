package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = Append(s)
	fmt.Println(s)
}

func Append(s []int) []int {
	n := len(s)
	newSlice := make([]int, len(s), len(s)*2+1)
	copy(newSlice, s)
	s = newSlice
	s = s[0 : n+1]
	s[n] = 4
	return s
}
