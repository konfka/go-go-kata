package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = Append(s)
	fmt.Println(s)
}

func Append(s []int) []int {
	newSlice := make([]int, len(s), cap(s)+1)
	copy(newSlice, s)
	newSlice = append(s, 4)
	return newSlice
}
