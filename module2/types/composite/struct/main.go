package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

type Location struct {
	Adress string
	City   string
	Index  string
}

func main() {
	user := User{
		Age:  13,
		Name: "Alexander"}
	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			144000,
			8900,
			55,
			34},
		Location: Location{
			Adress: "Нововатутинская 3-Я ул, 13, к. 2",
			City:   "Москва",
			Index:  "108836",
		},
	}
	fmt.Println(user)
	wallet := Wallet{
		RUR: 2500000,
		USD: 450000,
		BTC: 15,
		ETH: 100}
	fmt.Println(wallet)
	fmt.Println("wallet allocates:", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)
	fmt.Println(user2)
}
