package main

import (
	"fmt"
)

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	//Пришлось поправить нижнюю строку, так как ругался линтер со следующей ошибкой
	//S1021: should merge variable declaration with assignment on next line
	var u Userer = &User{
		ID:   34,
		Name: "Annet",
	}
	user := u.(*User)
	testUserName(*user)
}

func testUserName(u User) {
	if u.GetName() == "Annet" {
		fmt.Println("Success!")
	}
}
