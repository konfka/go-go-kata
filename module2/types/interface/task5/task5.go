package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	//var i Userer Пришлось удалить эту строку так как ругался линтер со следующей ошибкой
	//S1021: should merge variable declaration with assignment on next line
	i := User{}
	_ = i
	fmt.Println("Success!")
}
