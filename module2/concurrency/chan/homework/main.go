package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {

	ticker := time.NewTicker(500 * time.Millisecond)
	done := make(chan bool)
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		for num := range out {
			select {
			case <-done:
				return
			case <-ticker.C:
				a <- num
			}
		}
		close(a)
	}()

	go func() {
		for num := range out {
			select {
			case <-done:
				return
			case <-ticker.C:
				b <- num
			}
		}
		close(b)
	}()

	go func() {
		for num := range out {
			select {
			case <-done:
				return
			case <-ticker.C:
				c <- num
			}
		}
		close(c)

	}()

	mainChan := joinChannels(a, b, c)

	defer close(mainChan)

	for num := range mainChan {
		fmt.Println(num)
	}

	time.Sleep(1600 * time.Millisecond)
	ticker.Stop()
	done <- true
}
