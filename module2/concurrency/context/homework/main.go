package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"sync"
	"time"
)

func operation(ctx context.Context, ch chan int, num int) {
	select {
	case <-ctx.Done():
		return
	case <-time.After(10 * time.Millisecond):
		ch <- num
	}
}

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		defer cancel()
		for num := range out {
			operation(ctx, a, num)
		}
	}()

	go func() {
		defer cancel()
		for num := range out {
			operation(ctx, b, num)
		}
	}()

	go func() {
		defer cancel()
		for num := range out {
			operation(ctx, c, num)
		}
	}()

	mainChan := joinChannels(a, b, c)

	fmt.Println(len(mainChan))

	for num := range mainChan {
		fmt.Println(num)
	}

	close(mainChan)

	os.Exit(0)
}
