package main

import (
	"fmt"
	"time"
)

/*func main() {
	t := time.Now()
	go parceURL("http://example.com/")
	parceURL("http://youtube.com/")

	fmt.Printf("Parcing completed. Time Elapesed: %.2f seconds\n", time.Since(t).Seconds())
}
func parceURL(url string) {
	for i := 0; i < 5; i++ {
		latency := rand.Intn(500) + 500
		time.Sleep(time.Duration(latency) * time.Millisecond)

		fmt.Printf("Parsing<%s> - Step %d - latency %d ms\n", url, i+1, latency)
	}
}*/

/*func main() {
	message := make(chan string)

	go func() {
		time.Sleep(2 * time.Second)

	}()
	fmt.Println(<-message)
}*/

/*
	func main() {
		message := make(chan string)
		go func() {
			for i := 1; i <= 10; i++ {
				message <- fmt.Sprintf("%d", i)
				time.Sleep(time.Millisecond * 500)
			}
			close(message)
		}()
		for {
			for msg := range message {
				fmt.Println(msg)
			}
		}
	}
*/
/*func main() {
	message := make(chan string, 2)
	message <- "hello"
	fmt.Println(<-message)
	message <- "world"
	message <- "!"
	fmt.Println(<-message)
	fmt.Println(<-message)
}*/
/*func main() {
	urls := []string{
		"https://google.com/",
		"https://youtube.com/",
		"https://github.com/",
		"https://mail.com/",
		"https://medium.com/",
	}

	var wg sync.WaitGroup
	for _, url := range urls {
		wg.Add(1)
		go func(url string) {
			doHTTP(url)
			wg.Done()
		}(url)

	}
	wg.Wait()
}

func doHTTP(url string) {
	t := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Failed to get <%s>: %s\n", url, err.Error())
	}
	defer resp.Body.Close()
	fmt.Printf("<%s> - Status code [%d] - Latency %d	ms\n", url, resp.StatusCode, time.Since(t).Milliseconds())
}*/

func main() {
	message1 := make(chan string)
	message2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "Прошло полсекунды"
		}
	}()
	go func() {
		for {
			time.Sleep(time.Second * 2)
			message2 <- "Прошло две секунды"
		}
	}()
	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
	}
}
