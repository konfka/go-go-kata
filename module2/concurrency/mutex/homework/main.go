package main

import (
	"fmt"
	"sync"

	"golang.org/x/sync/errgroup"
)

type Cache struct {
	data map[string]interface{}
	init bool
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
	}
}

func (c *Cache) Set(key string, v interface{}, mutex *sync.Mutex) error {
	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}
	c.data[key] = v

	return nil
}

func (c *Cache) Get(key string) interface{} {
	if !c.init {
		return nil
	}
	return c.data[key]
}

func main() {
	cache := NewCache()
	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}

	var eg errgroup.Group
	var mutex sync.Mutex
	for i := range keys {
		mutex.Lock()
		idx := i
		eg.Go(func() error {
			return cache.Set(keys[idx], idx, &mutex)
		})
		mutex.Unlock()
	}

	err := eg.Wait()
	if err != nil {
		panic(err)
	}
}
