package main

import (
	"fmt"
	"unicode"
)

var flag = true

func IsRusByUnicode(str string) bool {
	for _, r := range str {
		if unicode.Is(unicode.Cyrillic, r) {
			flag = true
		} else {
			flag = false
		}
		if !flag {
			return false
		}
	}
	return true
}

func Greet(name string) string {
	switch {
	case IsRusByUnicode(name):
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	default:
		return fmt.Sprintf("Hello %s, you welcome!", name)
	}
}
