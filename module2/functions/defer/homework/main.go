package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func (f *MergeDictsJob) True() {
	f.IsFinished = true
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	defer job.True()
	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}
	job.Merged = make(map[string]string)
	for _, v := range job.Dicts {
		if v == nil {
			return job, errNilDict
		}
		for key, value := range v {
			job.Merged[key] = value
		}
	}
	return job, nil
}

func main() {
	job1 := MergeDictsJob{}
	job2 := MergeDictsJob{
		Dicts:      []map[string]string{{"a": "b"}, nil},
		Merged:     nil,
		IsFinished: false,
	}
	job3 := MergeDictsJob{
		Dicts:      []map[string]string{{"a": "b"}, {"b": "c"}},
		Merged:     nil,
		IsFinished: false,
	}
	mapa := []MergeDictsJob{job1, job2, job3}
	for i := range mapa {
		m, err := ExecuteMergeDictsJob(&mapa[i])
		switch {
		case err != nil:
			fmt.Println(err)
		default:
			fmt.Println(*m)
		}
	}
}
