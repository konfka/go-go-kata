package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/konfka/go-go-kata/module4/webserver/http/homework/internal/usecase/repository"
	"gitlab.com/konfka/go-go-kata/module4/webserver/http/homework/internal/usecase/service"

	"gitlab.com/konfka/go-go-kata/module4/webserver/http/homework/internal/handlers"
)

func createChannel() (chan os.Signal, func()) {
	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	return stopCh, func() {
		close(stopCh)
	}
}

func start(addr string, router *http.Handler) {
	log.Println("Starting application")
	if err := http.ListenAndServe(addr, *router); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatalln(err)
	} else {
		log.Println("Application stopped gracefully")
	}
}

func shutdown(ctx context.Context, server *http.Server) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig

		// Сигнал о завершении с ожиданием в 30 секунд для завершения текущих процессов
		shutdownCtx, cancel2 := context.WithTimeout(ctx, 30*time.Second)
		defer cancel2()
		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("Graceful shutdown timed out... forcing exit.")
			}
		}()
	}()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalln(err)
	} else {
		log.Println("Application shutdowned")
	}
}

func main() {
	hand := handlers.NewHandler(service.NewService(*repository.NewUserRepository()))
	server := http.Server{Addr: ":8080", Handler: hand.Routes()}
	go start(server.Addr, &server.Handler)

	stopCh, closeCh := createChannel()
	defer closeCh()
	log.Println("Notified:", <-stopCh)

	shutdown(context.Background(), &server)
}
