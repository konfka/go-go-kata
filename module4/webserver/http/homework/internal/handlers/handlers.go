package handlers

import (
	"encoding/json"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/konfka/go-go-kata/module4/webserver/http/homework/internal/usecase/service"

	"github.com/go-chi/chi/v5"
)

type Handler struct {
	service service.UserService
}

func NewHandler(service service.UserService) *Handler {
	return &Handler{service: service}
}

func (h *Handler) Routes() *chi.Mux {
	r := chi.NewRouter()
	r.Get("/", h.Greeting)
	r.Get("/users", h.AllUsers)
	r.Post("/users", h.AllUsers)
	r.Get("/users/{id}", h.GetUser)
	r.Get("/public/{filename}", h.GetFile)
	r.Post("/upload", h.FileUpload)

	return r
}

func (h *Handler) Greeting(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte(`{"message":"Hello friend!"}`))
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(`{"message":"Hello friend!"}`)
}

func (h *Handler) AllUsers(w http.ResponseWriter, r *http.Request) {

	usersBytes, err := json.Marshal(h.service.GetUsers())
	if err != nil {
		log.Fatalln(err)
	}

	_, err = w.Write(usersBytes)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(string(usersBytes))
}

func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		log.Fatalln(err)
	}

	pers, err := h.service.GetUserByID(id)
	if err != nil {
		log.Fatalln(err)
	}

	usersBytes, err := json.Marshal(pers)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = w.Write(usersBytes)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println(string(usersBytes))
}

func (h *Handler) GetFile(w http.ResponseWriter, r *http.Request) {

	filename := chi.URLParam(r, "filename")
	file, err := os.Open("./module4/webserver/http/homework/public/" + filename)
	if err != nil {
		log.Println(err)
	}

	defer func(file *os.File) {
		err = file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(file)

	read, err := io.ReadAll(file)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = w.Write(read)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(string(read))
}

func (h *Handler) FileUpload(w http.ResponseWriter, r *http.Request) {

	loadedFile, header, err := r.FormFile("upload")
	if err != nil {
		log.Fatalln(err)
	}
	defer func(loadedFile multipart.File) {
		err = loadedFile.Close()
		if err != nil {
			log.Fatalln(err)
		}
	}(loadedFile)

	file, err := os.OpenFile("./module4/webserver/http/homework/public/"+header.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Println(err)
	}
	defer func(file *os.File) {
		err = file.Close()
		if err != nil {
			log.Fatalln(err)
		}
	}(file)

	_, err = io.Copy(file, loadedFile)
	if err != nil {
		log.Fatalln(err)
	}

	/*read, err := io.ReadAll(file)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = w.Write(read)
	if err != nil {
		log.Fatalln(err)
	}*/
}

/*func (h *Handler) createChannel() (chan os.Signal, func()) {
	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	return stopCh, func() {
		close(stopCh)
	}
}

func (h *Handler) start(addr string, router *http.Handler) {
	log.Println("Starting application")
	if err := http.ListenAndServe(addr, *router); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatalln(err)
	} else {
		log.Println("Application stopped gracefully")
	}
}

func (h *Handler) shutdown(ctx context.Context, server *http.Server) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig

		// Сигнал о завершении с ожиданием в 30 секунд для завершения текущих процессов
		shutdownCtx, _ := context.WithTimeout(ctx, 30*time.Second)

		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("Graceful shutdown timed out... forcing exit.")
			}
		}()
	}()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalln(err)
	} else {
		log.Println("Application shutdowned")
	}
}
*/
