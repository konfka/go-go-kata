package service

import (
	"gitlab.com/konfka/go-go-kata/module4/webserver/http/homework/internal/entity"
	"gitlab.com/konfka/go-go-kata/module4/webserver/http/homework/internal/usecase/repository"
)

type UserService interface {
	GetUsers() []entity.Person
	GetUserByID(id int) (entity.Person, error)
}

type UserServ struct {
	repo repository.UsersRepo
}

func NewService(repo repository.UsersRepo) *UserServ {
	return &UserServ{repo: repo}
}

func (us *UserServ) GetUsers() []entity.Person {
	return us.repo.GetUsers()
}

func (us *UserServ) GetUserByID(id int) (entity.Person, error) {
	return us.repo.GetUserByID(id)
}
