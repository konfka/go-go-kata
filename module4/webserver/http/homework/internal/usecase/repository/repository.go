package repository

import (
	"gitlab.com/konfka/go-go-kata/module4/webserver/http/homework/internal/entity"
)

var listPerson = []entity.Person{
	entity.Person{
		Id:        1,
		Name:      "Slack",
		Character: "Harry Potter",
	},
	entity.Person{
		Id:        2,
		Name:      "Anna",
		Character: "John Weasley",
	},
	entity.Person{
		Id:        3,
		Name:      "Bob",
		Character: "Malfoy",
	},
}

type UserRepository interface {
	GetUsers() []entity.Person
	GetUserById(id int) (entity.Person, error)
}

type UsersRepo struct {
	users []entity.Person
}

func NewUserRepository() *UsersRepo {
	return &UsersRepo{users: listPerson}
}

func (ur *UsersRepo) GetUsers() []entity.Person {
	return ur.users
}

func (ur *UsersRepo) GetUserByID(id int) (entity.Person, error) {
	for _, v := range ur.users {
		if v.Id == id {
			return v, nil
		}
	}
	return entity.Person{}, nil
}
