package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

/*func main() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer ln.Close()

	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}

		go func(conn net.Conn) {
			defer conn.Close()

			data := make([]byte, 1024)
			n, err := conn.Read(data)
			if err != nil {
				fmt.Println(err)
				return
			}
			fmt.Println(string(data[:n]))
		}(conn)

	}

}
*/

func handleConnection(conn net.Conn) {
	defer func(conn net.Conn) {
		err := conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(conn)
	reader := bufio.NewReader(conn)

	// Read the request
	request, _ := reader.ReadString('\n')
	fmt.Println(request)

	// Parse the request
	parts := strings.Split(request, " ")
	if len(parts) < 2 {
		return
	}
	method := parts[0]
	path := parts[1]

	// Write the response
	switch {

	case method == "GET" && path == "/":
		m := "HTTP/1.1 200 OK\n"
		m = m + "Server: nginx/1.19.2\n"
		m = m + "Date: Mon, 19 Oct 2020 13:13:29 GMT\n"
		m = m + "Content-Type: text/html\n"
		m = m + "Content-Length: 98\n" // указываем размер контента
		m = m + "Last-Modified: Mon, 19 Oct 2020 13:13:13 GMT\n"
		m = m + "Connection: keep-alive\n"
		m = m + "ETag: \"5f8d90e9-62\"\n"
		m = m + "Accept-Ranges: bytes\n"
		m = m + "\n"
		m = m + "<!DOCTYPE html>\n"
		m = m + "<html>\n"
		m = m + "<head>\n"
		m = m + "<title>Webserver</title>\n"
		m = m + "</head>\n"
		m = m + "<body>\n"
		m = m + "hello world\n"
		m = m + "</body>\n"
		m = m + "</html>\n"
		_, err := conn.Write([]byte(m))
		if err != nil {
			log.Fatal(err)
		}

	case method == "POST" && path == "/upload":
		file, _ := os.Create("uploaded_file.txt")
		defer func(file *os.File) {
			err := file.Close()
			if err != nil {
				log.Fatal(err)
			}
		}(file)
		_, err := io.Copy(file, reader)
		if err != nil {
			log.Fatal(err)
		}

		_, err = fmt.Fprintf(conn, "HTTP/1.1 200 OK\r\n\r\nFile uploaded successfully!")
		if err != nil {
			log.Fatal(err)
		}

	default:
		_, err := fmt.Fprintf(conn, "HTTP/1.1 404 Not Found\r\n\r\n")
		if err != nil {
			log.Fatal(err)
		}
	}
}

func main() {
	listener, _ := net.Listen("tcp", ":8080")
	defer func(listener net.Listener) {
		err := listener.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(listener)

	for {
		conn, _ := listener.Accept()
		go handleConnection(conn)
	}
}
