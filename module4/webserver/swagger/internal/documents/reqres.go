// nolint
package documents

import (
	"bytes"

	entity2 "gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

//go:generate swagger generate spec -o /Users/svecsofa/go/src/gitlab.com/konfka/go-go-kata/module4/webserver/swagger/public/swagger.json

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	Body entity2.Pets
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body entity2.Pets
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body entity2.Pets
}

// swagger:route POST /pet/{id}/uploadImage pet petUploadImageRequest
// Добавление картинки питомца по ID.
// responses:
//   200: petUploadImageResponse

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct {
	// PetId of pet to update
	// required: true
	// In: path
	PetId int64 `json:"id"`

	// file to upload
	// in: formData
	// swagger:file
	File *bytes.Buffer `json:"file"`
}

// swagger:route PUT /pet pet petUpdatePetRequest
// Обновление существующего питомца.
// responses:
//   400: description: Invalid ID supplied
//   404: description: Pet not found
//   405: description: Validation exception

// swagger:parameters petUpdatePetRequest
type petUpdatePetRequest struct {

	// Pet object that needs to be added to the store
	// required: true
	// in:body
	Body entity2.Pets
}

// swagger:response petUpdatePetResponse
type petUpdatePetResponse struct {
	// in:body
	Body entity2.Pets
}

// swagger:route GET /pet/findByStatus pet petFindByStatusRequest
// Поиск питомцев по списку статусов.
// responses:
//   200: description: successful operation
//   400: description: invalid status value

// swagger:parameters petFindByStatusRequest
type petFindByStatusRequest struct {
	// Statuses of pet
	// required: true
	// enum: pending,sold,available
	// in:query
	Status string `json:"status"`
}

// swagger:response petFindByStatusResponse
type petFindByStatusResponse struct {
	// in:body
	Body []entity2.Pets
}

// swagger:route POST /pet/{id} pet petUploadWithDataRequest
// Обновление данных питомца.
// responses:
//   200: description: successful operation
//	 400: description: invalid input

// swagger:parameters petUploadWithDataRequest
type petUploadWithDataRequest struct {
	// ID of an item
	// required:true
	// In: path
	ID int `json:"id"`

	// Updated name of the pet
	// in: formData
	Name string `json:"name"`

	// Updated status of the pet
	// in: formData
	Status string `json:"status"`
}

// swagger:route DELETE /pet/{id} pet petDeletePetRequest
// Удаление питомца.
// responses:
//   200: description: successful operation
//	 400: description: invalid id supplied
//	 404: description: pet not found

// swagger:parameters petDeletePetRequest
type petDeletePetRequest struct {
	// ID of an item
	// required:true
	// In: path
	ID int `json:"id"`
}

// swagger:route DELETE /store/order/{id} store storeDeleteOrderRequest
// Удаление заказа.
// responses:
//   200: description: successful operation
//	 400: description: invalid id supplied
//	 404: description: order not found

// swagger:parameters storeDeleteOrderRequest
type storeDeleteOrderRequest struct {
	// ID of an order
	// required:true
	// In: path
	ID string `json:"id"`
}

// swagger:route POST /store/order store storeCreateOrderRequest
// Добавление заказа.
// responses:
//   200: description: successful operation
//   400: description: invalid order

// swagger:parameters storeCreateOrderRequest
type storeCreateOrderRequest struct {
	// in:body
	Body entity2.Order
}

// swagger:response storeCreateOrderResponse
type storeCreateOrderResponse struct {
	// in:body
	Body entity2.Order
}

// swagger:route GET /store/inventory store storeStatusQuantitiesRequest
// Количество заказов с определенным статусом.
// responses:
//   200: description: successful operation
//   500: description: invalid writing response

// swagger:response storeStatusQuantitiesResponse
type storeStatusQuantitiesResponse struct {
	// in:body
	Map map[string]int
}

// swagger:route GET /store/order/{id} store storeOrderGetByIDRequest
// Получение заказа по id.
// responses:
//   200: storeOrderGetByIDResponse
//	 400: description: invalid id supplied
//	 404: description: order not found

// swagger:parameters storeOrderGetByIDRequest
type storeOrderGetByIDRequest struct {
	// ID of an order
	// required: true
	// In: path
	ID string `json:"id"`
}

// swagger:response storeOrderGetByIDResponse
type storeOrderGetByIDResponse struct {
	// in:body
	Body entity2.Order
}

// swagger:route POST /user/createWithArray user userCreateWithArrayRequest
// Создание списка пользователей по заданному входящему списку.
// responses:
//   200: description: successful operation
//   400: description: invalid writing

// swagger:parameters userCreateWithArrayRequest
type userCreateWithArrayRequest struct {
	// in:body
	Body []entity2.User
}

// swagger:route POST /user/createWithList user userCreateWithListRequest
// Создание списка пользователей по заданному входящему списку.
// responses:
//   200: description: successful operation
//   400: description: invalid writing

// swagger:parameters userCreateWithListRequest
type userCreateWithListRequest struct {
	// in:body
	Body []entity2.User
}

// swagger:route GET /user/{username} user userGetByUsernameRequest
// Получение пользователя по username.
// responses:
//   200: description: successful operation
//	 400: description: invalid username supplied
//	 404: description: user not found

// swagger:parameters userGetByUsernameRequest
type userGetByUsernameRequest struct {
	// The name that needs to be fetched.
	// required: true
	// In: path
	Username string `json:"username"`
}

// swagger:response userGetByUsernameResponse
type userGetByUsernameResponse struct {
	// in:body
	Body entity2.User
}

// swagger:route PUT /user/{username} user userUpdateUserRequest
// Обновление пользователя.
// responses:
//   400: description: Invalid username supplied
//   404: description: User not found

// swagger:parameters userUpdateUserRequest
type userUpdateUserRequest struct {
	// Name that need to be updated
	// required: true
	// in:path
	Username string `json:"username"`

	// Updated user object
	// in:body
	Body entity2.User
}

// swagger:route DELETE /user/{username} user userDeleteRequest
// Удаление пользователя.
// responses:
//   200: description: successful operation
//	 400: description: invalid username supplied
//	 404: description: user not found

// swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	// The name that needs to be deleted
	// required:true
	// In: path
	Username string `json:"username"`
}

// swagger:route POST /user user userCreateUserRequest
// Создание пользователя.
// responses:
//   200: description: successful operation
//   400: description: invalid writing

// swagger:parameters userCreateUserRequest
type userCreateUserRequest struct {
	// Created user object
	// in:body
	Body entity2.User
}
