package main

import (
	"context"
	"encoding/json"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"time"

	"gitlab.com/konfka/go-go-kata/module4/webserver/swagger/repository"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	entity2 "gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func main() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	petController := NewPetController()
	r.Post("/pet", petController.PetCreate)
	r.Get("/pet/{petID}", petController.PetGetByID)
	r.Post("/pet/{petID}/uploadImage", petController.UploadImage)
	r.Put("/pet", petController.UpdatePet)
	r.Get("/pet/findByStatus", petController.FindByStatus)
	r.Post("/pet/{petID}", petController.UploadWithData)
	r.Delete("/pet/{petID}", petController.DeletePet)

	orderController := NewStoreController()
	r.Delete("/store/order/{orderID}", orderController.DeleteOrder)
	r.Post("/store/order", orderController.OrderCreate)
	r.Get("/store/order/{orderID}", orderController.OrderGetByID)
	r.Get("/store/inventory", orderController.OrderStatusQuantities)

	userController := NewUserController()
	r.Post("/user/createWithArray", userController.CreateWithArray)
	r.Post("/user/createWithList", userController.CreateWithList)
	r.Get("/user/{username}", userController.GetByUsername)
	r.Delete("/user/{username}", userController.Delete)
	r.Post("/user", userController.CreateUser)
	r.Put("/user/{username}", userController.UpdateUser)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./module4/webserver/swagger/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Printf("server started on port %s ", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

type PetController struct { // Pet контроллер
	storage repository.PetStorager
}

func NewPetController() *PetController { // конструктор нашего контроллера
	return &PetController{storage: repository.NewPetStorage()}
}

type StoreController struct {
	storage repository.StoreStorager
}

func NewStoreController() *StoreController {
	return &StoreController{storage: repository.NewStoreStorage()}
}

type UserController struct {
	storage repository.UserStorager
}

func NewUserController() *UserController {
	return &UserController{storage: repository.NewUserStorage()}
}

func (p *PetController) PetCreate(w http.ResponseWriter, r *http.Request) {
	var pet entity2.Pets
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet = p.storage.Create(pet) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) UploadImage(w http.ResponseWriter, r *http.Request) {

	petIDRaw := chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	file, header, err := r.FormFile("file")
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	defer file.Close()

	fileName := strconv.Itoa(petID) + ".*" + filepath.Ext(header.Filename)
	f, err := os.CreateTemp("module4/webserver/swagger/images/", fileName)
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	defer f.Close()

	_, err = io.Copy(f, file)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	_, err = p.storage.UploadImage(petID, "/image"+filepath.Base(f.Name()))
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      entity2.Pets
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.storage.GetByID(petID) // пытаемся получить Pet по id
	if err != nil {                     // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) UpdatePet(w http.ResponseWriter, r *http.Request) {

	var pet entity2.Pets

	err := json.NewDecoder(r.Body).Decode(&pet)

	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	defer r.Body.Close()

	updated, err := p.storage.Update(pet)
	if err != nil { // в случае ошибки отправляем код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = json.NewEncoder(w).Encode(updated)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

}

func (p *PetController) FindByStatus(w http.ResponseWriter, r *http.Request) {

	status := r.URL.Query().Get("status")

	pets := p.storage.FindByStatus(status)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(pets)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) UploadWithData(w http.ResponseWriter, r *http.Request) {

	petIDRaw := chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	name := r.FormValue("name")

	status := r.FormValue("status")

	err = p.storage.UpdateWithData(petID, name, status)
	if err != nil {
		log.Fatalln(err)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

}

func (p *PetController) DeletePet(w http.ResponseWriter, r *http.Request) {
	petIDRaw := chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = p.storage.Delete(petID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
}

func (s *StoreController) DeleteOrder(w http.ResponseWriter, r *http.Request) {
	orderIDRaw := chi.URLParam(r, "orderID") // получаем orderID из chi router

	orderID, err := strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                          // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = s.storage.Delete(orderID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
}

func (s *StoreController) OrderCreate(w http.ResponseWriter, r *http.Request) {

	var order entity2.Order
	err := json.NewDecoder(r.Body).Decode(&order) // считываем приходящий json из *http.Request в структуру Order

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order = s.storage.Create(order) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(order)                           // записываем результат Order json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (s *StoreController) OrderGetByID(w http.ResponseWriter, r *http.Request) {

	var order entity2.Order

	orderIDRaw := chi.URLParam(r, "orderID") // получаем orderID из chi router

	orderID, err := strconv.Atoi(orderIDRaw) // конвертируем в int

	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order, err = s.storage.GetByID(orderID) // пытаемся получить Order по id
	if err != nil {                         // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (s *StoreController) OrderStatusQuantities(w http.ResponseWriter, r *http.Request) {

	statuses := s.storage.StatusQuantities()

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(statuses)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (u *UserController) CreateUser(w http.ResponseWriter, r *http.Request) {

	var user entity2.User

	err := json.NewDecoder(r.Body).Decode(&user) // считываем приходящий json из *http.Request в структуру User

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	u.storage.CreateUser(user) // создаем запись в нашем storage

}

func (u *UserController) CreateWithArray(w http.ResponseWriter, r *http.Request) {

	var users []entity2.User

	err := json.NewDecoder(r.Body).Decode(&users)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	u.storage.CreateWithArray(users)
}

func (u *UserController) CreateWithList(w http.ResponseWriter, r *http.Request) {

	var users []entity2.User

	err := json.NewDecoder(r.Body).Decode(&users)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	u.storage.CreateWithList(users)
}

func (u *UserController) Delete(w http.ResponseWriter, r *http.Request) {

	name := chi.URLParam(r, "username")

	err := u.storage.Delete(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (u *UserController) GetByUsername(w http.ResponseWriter, r *http.Request) {

	name := chi.URLParam(r, "username")

	user, err := u.storage.GetByUsername(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (u *UserController) UpdateUser(w http.ResponseWriter, r *http.Request) {

	var user entity2.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	name := chi.URLParam(r, "username")

	err = u.storage.UpdateUser(name, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}
