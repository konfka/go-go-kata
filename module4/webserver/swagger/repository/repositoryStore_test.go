package repository

import (
	"reflect"
	"testing"

	"gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

func TestStoreStorage_Create(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		statuses           map[string]int
		autoIncrementCount int
	}
	type args struct {
		order entity.Order
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Order
	}{
		{
			name: "First test",
			fields: fields{
				data:          []*entity.Order{},
				primaryKeyIDx: make(map[int]*entity.Order),
				statuses:      make(map[string]int),
			},
			args: args{
				order: entity.Order{
					ID:       0,
					PetID:    0,
					Quantity: 0,
					ShipDate: "",
					Status:   "placed",
					Complete: false,
				},
			},
			want: entity.Order{
				ID:       0,
				PetID:    0,
				Quantity: 0,
				ShipDate: "",
				Status:   "placed",
				Complete: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				statuses:           tt.fields.statuses,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := s.Create(tt.args.order); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		statuses           map[string]int
		autoIncrementCount int
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "First test",
			fields: fields{
				data: []*entity.Order{
					{
						ID:       0,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
				},
				primaryKeyIDx: map[int]*entity.Order{
					0: {
						ID:       0,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
				},
				statuses: make(map[string]int),
			},
			args: args{
				id: 0,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				statuses:           tt.fields.statuses,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := s.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStoreStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		statuses           map[string]int
		autoIncrementCount int
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Order
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.Order{
					{
						ID:       0,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
				},
				primaryKeyIDx: map[int]*entity.Order{
					0: {
						ID:       0,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
				},
				statuses: make(map[string]int),
			},
			args: args{id: 0},
			want: entity.Order{
				ID:       0,
				PetID:    0,
				Quantity: 0,
				ShipDate: "",
				Status:   "placed",
				Complete: false,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				statuses:           tt.fields.statuses,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := s.GetByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_StatusQuantities(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		statuses           map[string]int
		autoIncrementCount int
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]int
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.Order{
					{
						ID:       0,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
					{
						ID:       1,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
					{
						ID:       2,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "pending",
						Complete: false,
					},
				},
				primaryKeyIDx: map[int]*entity.Order{
					0: {
						ID:       0,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
					1: {
						ID:       1,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "placed",
						Complete: false,
					},
					2: {
						ID:       2,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "pending",
						Complete: false,
					},
				},
				statuses: map[string]int{
					"placed":  2,
					"pending": 1,
				},
			},
			want: map[string]int{
				"placed":  2,
				"pending": 1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				statuses:           tt.fields.statuses,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := s.StatusQuantities(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("StatusQuantities() = %v, want %v", got, tt.want)
			}
		})
	}
}
