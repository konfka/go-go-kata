package repository

import (
	"fmt"
	"sync"

	"gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

type PetStorager interface {
	Create(pet entity.Pets) entity.Pets
	Update(pet entity.Pets) (entity.Pets, error)
	Delete(petID int) error
	GetByID(petID int) (entity.Pets, error)
	UploadImage(petID int, URL string) (entity.Pets, error)
	FindByStatus(statuses string) []entity.Pets
	UpdateWithData(id int, name, status string) error
}

type PetStorage struct {
	data               []*entity.Pets
	primaryKeyIDx      map[int]*entity.Pets
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:          make([]*entity.Pets, 0, 13),
		primaryKeyIDx: make(map[int]*entity.Pets, 13),
	}
}

func (p *PetStorage) Create(pet entity.Pets) entity.Pets {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.autoIncrementCount++
	p.primaryKeyIDx[pet.ID] = &pet
	p.data = append(p.data, &pet)
	return pet
}

func (p *PetStorage) GetByID(petID int) (entity.Pets, error) {
	p.Lock()
	defer p.Unlock()
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}
	return entity.Pets{}, fmt.Errorf("pet not found")
}

func (p *PetStorage) UploadImage(petID int, URL string) (entity.Pets, error) {
	p.Lock()
	defer p.Unlock()
	if v, ok := p.primaryKeyIDx[petID]; ok {
		v.PhotoUrls = append(v.PhotoUrls, URL)
		return *v, nil
	}
	return entity.Pets{}, fmt.Errorf("pet not found")
}

func (p *PetStorage) Update(pet entity.Pets) (entity.Pets, error) {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[pet.ID]; !ok {
		return entity.Pets{}, fmt.Errorf("pet not found")
	}
	oldPet := p.primaryKeyIDx[pet.ID]
	p.primaryKeyIDx[pet.ID] = &pet
	for i := range p.data {
		if p.data[i] == oldPet {
			p.data[i] = &pet
			break
		}
	}

	return pet, nil
}

func (p *PetStorage) FindByStatus(status string) []entity.Pets {
	p.Lock()
	defer p.Unlock()

	pets := make([]entity.Pets, 0, 15)
	for _, v := range p.data {
		if v.Status == status {
			pets = append(pets, *v)
		}
	}
	return pets
}

func (p *PetStorage) UpdateWithData(id int, name, status string) error {
	p.Lock()
	defer p.Unlock()

	if _, ok := p.primaryKeyIDx[id]; !ok {
		return fmt.Errorf("pet not found")
	}
	oldPet := p.primaryKeyIDx[id]
	p.primaryKeyIDx[id].Name = name
	p.primaryKeyIDx[id].Status = status
	for i := range p.data {
		if p.data[i] == oldPet {
			p.data[i].Status = status
			p.data[i].Name = name
			break
		}
	}
	return nil
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()

	delete(p.primaryKeyIDx, petID)

	var firstPart = make([]*entity.Pets, 0, len(p.data))
	var secondPart = make([]*entity.Pets, 0, len(p.data))
	var i int
	for i = range p.data {
		if p.data[i].ID == petID {
			firstPart = p.data[:i]
			secondPart = p.data[i+1:]
			break
		}
	}
	if p.data[i].ID != petID {
		return fmt.Errorf("no pet with that id")
	}
	p.data = append(firstPart, secondPart...)
	return nil
}
