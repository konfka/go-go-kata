package repository

import (
	"reflect"
	"testing"

	"gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

func TestUserStorage_CreateUser(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		user entity.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "first test",
			fields: fields{
				data:          []*entity.User{},
				primaryKeyIDx: make(map[string]*entity.User),
			},
			args: args{
				entity.User{
					ID:         0,
					Username:   "",
					FirstName:  "",
					LastName:   "",
					Email:      "",
					Password:   "",
					Phone:      "",
					UserStatus: 0,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			u.CreateUser(tt.args.user)
		})
	}
}

func TestUserStorage_CreateWithArray(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		user []entity.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "first test",
			fields: fields{
				data:          []*entity.User{},
				primaryKeyIDx: make(map[string]*entity.User),
			},
			args: args{
				[]entity.User{
					{
						ID:         0,
						Username:   "Man",
						FirstName:  "Amogus",
						LastName:   "Aboba",
						Email:      "",
						Password:   "Trump",
						Phone:      "",
						UserStatus: 0,
					},
					{
						ID:         0,
						Username:   "Man",
						FirstName:  "Amogus",
						LastName:   "Aboba",
						Email:      "",
						Password:   "Trump",
						Phone:      "",
						UserStatus: 0,
					},
					{
						ID:         0,
						Username:   "Man",
						FirstName:  "Amogus",
						LastName:   "Aboba",
						Email:      "",
						Password:   "Trump",
						Phone:      "",
						UserStatus: 0,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			u.CreateWithArray(tt.args.user)
		})
	}
}

func TestUserStorage_CreateWithList(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		user []entity.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "first test",
			fields: fields{
				data:          []*entity.User{},
				primaryKeyIDx: make(map[string]*entity.User),
			},
			args: args{
				[]entity.User{
					{
						ID:         0,
						Username:   "Man",
						FirstName:  "Amogus",
						LastName:   "Aboba",
						Email:      "",
						Password:   "Trump",
						Phone:      "",
						UserStatus: 0,
					},
					{
						ID:         0,
						Username:   "Man",
						FirstName:  "Amogus",
						LastName:   "Aboba",
						Email:      "",
						Password:   "Trump",
						Phone:      "",
						UserStatus: 0,
					},
					{
						ID:         0,
						Username:   "Man",
						FirstName:  "Amogus",
						LastName:   "Aboba",
						Email:      "",
						Password:   "Trump",
						Phone:      "",
						UserStatus: 0,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			u.CreateWithList(tt.args.user)
		})
	}
}

func TestUserStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.User{
					{
						ID:         0,
						Username:   "Muzhik",
						FirstName:  "",
						LastName:   "",
						Email:      "",
						Password:   "",
						Phone:      "",
						UserStatus: 0,
					},
				},
				primaryKeyIDx: map[string]*entity.User{
					"Muzhik": {
						ID:         0,
						Username:   "Muzhik",
						FirstName:  "",
						LastName:   "",
						Email:      "",
						Password:   "",
						Phone:      "",
						UserStatus: 0,
					},
				},
			},
			args:    args{username: "Muzhik"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := u.Delete(tt.args.username); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserStorage_GetByUsername(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.User
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.User{
					{
						ID:         0,
						Username:   "Muzhik",
						FirstName:  "",
						LastName:   "",
						Email:      "",
						Password:   "",
						Phone:      "",
						UserStatus: 0,
					},
				},
				primaryKeyIDx: map[string]*entity.User{
					"Muzhik": {
						ID:         0,
						Username:   "Muzhik",
						FirstName:  "",
						LastName:   "",
						Email:      "",
						Password:   "",
						Phone:      "",
						UserStatus: 0,
					},
				},
			},
			args: args{username: "Muzhik"},
			want: entity.User{
				ID:         0,
				Username:   "Muzhik",
				FirstName:  "",
				LastName:   "",
				Email:      "",
				Password:   "",
				Phone:      "",
				UserStatus: 0},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := u.GetByUsername(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByUsername() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByUsername() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_UpdateUser(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		username string
		user     entity.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.User{
					{
						ID:         0,
						Username:   "Muzhik",
						FirstName:  "",
						LastName:   "",
						Email:      "",
						Password:   "",
						Phone:      "",
						UserStatus: 0,
					},
				},
				primaryKeyIDx: map[string]*entity.User{
					"Muzhik": {
						ID:         0,
						Username:   "Muzhik",
						FirstName:  "",
						LastName:   "",
						Email:      "",
						Password:   "",
						Phone:      "",
						UserStatus: 0,
					},
				},
			},
			args: args{username: "Muzhik",
				user: entity.User{
					ID:         0,
					Username:   "Muzhik",
					FirstName:  "Anybody",
					LastName:   "Nobody at all",
					Email:      "notyourbusiness@gmail.com",
					Password:   "password",
					Phone:      "yeees",
					UserStatus: 0,
				}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := u.UpdateUser(tt.args.username, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("UpdateUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
