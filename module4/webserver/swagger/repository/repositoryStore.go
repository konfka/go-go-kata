package repository

import (
	"fmt"
	"sync"

	"gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

type StoreStorager interface {
	Create(order entity.Order) entity.Order
	Delete(OrderID int) error
	GetByID(OrderID int) (entity.Order, error)
	StatusQuantities() map[string]int
}

type StoreStorage struct {
	data               []*entity.Order
	primaryKeyIDx      map[int]*entity.Order
	statuses           map[string]int
	autoIncrementCount int
	sync.Mutex
}

func NewStoreStorage() *StoreStorage {
	return &StoreStorage{
		data:          make([]*entity.Order, 0, 13),
		primaryKeyIDx: make(map[int]*entity.Order, 13),
		statuses:      make(map[string]int, 13),
	}
}

func (s *StoreStorage) Create(order entity.Order) entity.Order {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()

	order.ID = s.autoIncrementCount

	s.primaryKeyIDx[s.autoIncrementCount] = &order
	s.autoIncrementCount++

	_, ok := s.statuses[order.Status]
	if ok {
		s.statuses[order.Status]++
	} else {
		s.statuses[order.Status] = 1
	}

	s.data = append(s.data, &order)

	return order
}

func (s *StoreStorage) Delete(id int) error {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()

	delete(s.primaryKeyIDx, id)

	var firstPart = make([]*entity.Order, 0, len(s.data))
	var secondPart = make([]*entity.Order, 0, len(s.data))

	var i int
	for i = range s.data {
		if s.data[i].ID == id {
			s.statuses[s.data[i].Status]--
			firstPart = s.data[:i]
			secondPart = s.data[i+1:]
			break
		}
	}
	if s.data[i].ID != id {
		return fmt.Errorf("no order with that id")
	}
	s.data = append(firstPart, secondPart...)
	return nil
}

func (s *StoreStorage) GetByID(id int) (entity.Order, error) {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()
	for _, v := range s.primaryKeyIDx {
		if v.ID == id {
			return *v, nil
		}
	}
	return entity.Order{}, fmt.Errorf("order not found")
}

func (s *StoreStorage) StatusQuantities() map[string]int {
	s.Mutex.Lock()
	defer s.Mutex.Unlock()
	return s.statuses
}
