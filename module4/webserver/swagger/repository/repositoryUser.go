package repository

import (
	"fmt"
	"sync"

	"gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

type UserStorager interface {
	CreateUser(user entity.User)
	CreateWithArray(user []entity.User)
	CreateWithList(user []entity.User)
	Delete(username string) error
	GetByUsername(username string) (entity.User, error)
	UpdateUser(username string, user entity.User) error
}

type UserStorage struct {
	data               []*entity.User
	primaryKeyIDx      map[string]*entity.User
	autoIncrementCount int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:          make([]*entity.User, 0, 13),
		primaryKeyIDx: make(map[string]*entity.User, 13),
	}
}

func (u *UserStorage) CreateUser(user entity.User) {
	u.Lock()
	defer u.Unlock()

	user.ID = u.autoIncrementCount

	u.primaryKeyIDx[user.Username] = &user
	u.autoIncrementCount++

	u.data = append(u.data, &user)
}

func (u *UserStorage) CreateWithArray(user []entity.User) {
	u.Lock()
	defer u.Unlock()

	for _, v := range user {
		v.ID = u.autoIncrementCount

		u.primaryKeyIDx[v.Username] = &v
		u.autoIncrementCount++

		u.data = append(u.data, &v)
	}
}

func (u *UserStorage) CreateWithList(user []entity.User) {
	u.Lock()
	defer u.Unlock()

	for _, v := range user {
		v.ID = u.autoIncrementCount

		u.primaryKeyIDx[v.Username] = &v
		u.autoIncrementCount++

		u.data = append(u.data, &v)
	}
}

func (u *UserStorage) Delete(username string) error {
	u.Lock()
	defer u.Unlock()

	delete(u.primaryKeyIDx, username)

	var firstPart = make([]*entity.User, 0, len(u.data))
	var secondPart = make([]*entity.User, 0, len(u.data))

	var i int
	for i = range u.data {
		if u.data[i].Username == username {
			firstPart = u.data[:i]
			secondPart = u.data[i+1:]
			break
		}
	}
	if u.data[i].Username != username {
		return fmt.Errorf("no user with that username")
	}
	u.data = append(firstPart, secondPart...)
	return nil
}

func (u *UserStorage) GetByUsername(username string) (entity.User, error) {
	u.Lock()
	defer u.Unlock()
	for _, v := range u.primaryKeyIDx {
		if v.Username == username {
			return *v, nil
		}
	}
	return entity.User{}, fmt.Errorf("user not found")
}

func (u *UserStorage) UpdateUser(username string, user entity.User) error {

	u.Lock()
	defer u.Unlock()

	if _, ok := u.primaryKeyIDx[username]; !ok {
		return fmt.Errorf("pet not found")
	}

	oldPet := u.primaryKeyIDx[username]
	u.primaryKeyIDx[username] = &user
	for i := range u.data {
		if u.data[i] == oldPet {
			u.data[i] = &user
			break
		}
	}
	return nil
}
