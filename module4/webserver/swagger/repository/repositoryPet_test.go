package repository

import (
	"reflect"
	"testing"

	"gitlab.com/konfka/go-go-kata/module4/webserver/swagger/internal/entity"
)

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*entity.Pets
		primaryKeyIDx      map[int]*entity.Pets
		autoIncrementCount int
	}
	type args struct {
		pet entity.Pets
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Pets
	}{
		{
			name: "first test",
			fields: fields{
				data:               []*entity.Pets{},
				primaryKeyIDx:      make(map[int]*entity.Pets),
				autoIncrementCount: 0,
			},

			args: args{
				pet: entity.Pets{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []entity.Category{},
					Status: "active",
				},
			},
			want: entity.Pets{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var got entity.Pets
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetStorage_GetByID(t *testing.T) {

	user := entity.Pets{
		ID: 0,
		Category: entity.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Alma",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
			"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
		},
		Tags:   []entity.Category{},
		Status: "active",
	}

	p := &PetStorage{
		data:          make([]*entity.Pets, 13),
		primaryKeyIDx: make(map[int]*entity.Pets, 13),
	}

	type fields struct {
		data               []*entity.Pets
		primaryKeyIDx      map[int]*entity.Pets
		autoIncrementCount int
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Pets
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data:               []*entity.Pets{},
				primaryKeyIDx:      make(map[int]*entity.Pets),
				autoIncrementCount: 0,
			},
			args: args{
				petID: 0,
			},
			want: entity.Pets{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			p.Create(user)
			got, err := p.GetByID(tt.args.petID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_UploadImage(t *testing.T) {

	user := entity.Pets{
		ID: 0,
		Category: entity.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Alma",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
		},
		Tags:   []entity.Category{},
		Status: "active",
	}

	p := &PetStorage{
		data:          make([]*entity.Pets, 13),
		primaryKeyIDx: make(map[int]*entity.Pets, 13),
	}

	type fields struct {
		data               []*entity.Pets
		primaryKeyIDx      map[int]*entity.Pets
		autoIncrementCount int
	}
	type args struct {
		petID int
		URL   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Pets
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data:               []*entity.Pets{},
				primaryKeyIDx:      make(map[int]*entity.Pets),
				autoIncrementCount: 0,
			},
			args: args{
				petID: 0,
				URL:   "https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
			},
			want: entity.Pets{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p.Create(user)
			got, err := p.UploadImage(tt.args.petID, tt.args.URL)
			if (err != nil) != tt.wantErr {
				t.Errorf("UploadImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UploadImage() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {

	user := entity.Pets{
		ID: 0,
		Category: entity.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Alma",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
			"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
		},
		Tags:   []entity.Category{},
		Status: "active",
	}

	p := &PetStorage{
		data:          make([]*entity.Pets, 13),
		primaryKeyIDx: make(map[int]*entity.Pets, 13),
	}

	type fields struct {
		data               []*entity.Pets
		primaryKeyIDx      map[int]*entity.Pets
		autoIncrementCount int
	}
	type args struct {
		pet entity.Pets
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Pets
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data:               []*entity.Pets{},
				primaryKeyIDx:      make(map[int]*entity.Pets),
				autoIncrementCount: 0,
			},
			args: args{
				pet: entity.Pets{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "UPDATED",
					},
					Name: "UPDATED",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []entity.Category{},
					Status: "active",
				},
			},
			want: entity.Pets{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "UPDATED",
				},
				Name: "UPDATED",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p.Create(user)
			got, err := p.Update(tt.args.pet)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_FindByStatus(t *testing.T) {

	type fields struct {
		data               []*entity.Pets
		primaryKeyIDx      map[int]*entity.Pets
		autoIncrementCount int
	}
	type args struct {
		status string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []entity.Pets
	}{
		{
			name: "Test with matching status",
			fields: fields{
				// Set up the initial state of PetStorage
				data: []*entity.Pets{
					{Status: "available"},
					{Status: "pending"},
					{Status: "sold"},
				},
				primaryKeyIDx:      make(map[int]*entity.Pets),
				autoIncrementCount: 0,
			},
			args: args{
				status: "available", // Provide the status for testing
			},
			want: []entity.Pets{
				{Status: "available"}, // Define the expected result
			},
		}, {
			name: "Test without matching status",
			fields: fields{
				// Set up the initial state of PetStorage
				data: []*entity.Pets{
					{Status: "available"},
					{Status: "pending"},
					{Status: "sold"},
				},
			},
			args: args{
				status: "adopted", // Provide a status that does not exist in the data
			},
			want: []entity.Pets{}, // Expect an empty result as there are no pets with the "adopted" status
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := p.FindByStatus(tt.args.status); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_UpdateWithData(t *testing.T) {

	user1 := entity.Pets{
		ID: 0,
		Category: entity.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Alma",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
		},
		Tags:   []entity.Category{},
		Status: "available",
	}
	user2 := entity.Pets{
		ID: 1,
		Category: entity.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Alma",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
		},
		Tags:   []entity.Category{},
		Status: "available",
	}

	p := &PetStorage{
		data:          make([]*entity.Pets, 13),
		primaryKeyIDx: make(map[int]*entity.Pets, 13),
	}
	p.Create(user1)
	p.Create(user2)

	type fields struct {
		data               []*entity.Pets
		primaryKeyIDx      map[int]*entity.Pets
		autoIncrementCount int
	}
	type args struct {
		id     int
		name   string
		status string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data:               []*entity.Pets{},
				primaryKeyIDx:      make(map[int]*entity.Pets),
				autoIncrementCount: 0,
			},
			args: args{
				id:     1,
				name:   "Koko",
				status: "available",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			if err := p.UpdateWithData(tt.args.id, tt.args.name, tt.args.status); (err != nil) != tt.wantErr {
				t.Errorf("UpdateWithData() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {

	type fields struct {
		data               []*entity.Pets
		primaryKeyIDx      map[int]*entity.Pets
		autoIncrementCount int
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Delete existing pet",
			fields: fields{
				data: []*entity.Pets{
					{
						ID:        1,
						Name:      "Charlie",
						Status:    "available",
						Category:  entity.Category{ID: 1, Name: "Category 1"},
						PhotoUrls: []string{"url1", "url2"},
						Tags: []entity.Category{
							{ID: 1, Name: "Tag 1"},
							{ID: 2, Name: "Tag 2"},
						},
					},
				},
				autoIncrementCount: 2,
			},
			args:    args{petID: 1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := p.Delete(tt.args.petID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
