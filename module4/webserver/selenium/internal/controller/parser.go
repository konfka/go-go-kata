package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/konfka/go-go-kata/module4/webserver/selenium/internal/entity"

	"github.com/PuerkitoBio/goquery"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

const maxTries = 5
const HabrCareerLink = "https://career.habr.com"
const PageVacancies = 25

type VacancyPars struct {
	driver selenium.WebDriver
}

func NewVacancyPars() *VacancyPars {
	return &VacancyPars{}
}

func (v *VacancyPars) Parse(query string) ([]entity.Vacancy, error) {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	var err error
	// прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		v.driver, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	// после окончания программы завершаем работу драйвера
	defer func(driver selenium.WebDriver) {
		err = driver.Close()
		if err != nil {

		}
	}(v.driver)

	count, err := v.VacancyCount(query)
	if err != nil {
		log.Fatal(err)
	}

	links, err := v.Vacancies(count, query)
	if err != nil {
		log.Fatal(err)
	}

	result := make([]entity.Vacancy, 0, len(links))

	for _, link := range links {
		val, err := v.JSONBySelector(link)
		if err != nil {
			continue
		}

		result = append(result, val)
	}

	return result, nil

}

func (v *VacancyPars) VacancyCount(query string) (int, error) {
	// сразу обращаемся к странице с поиском вакансии по запросу
	page := 1 // номер страницы

	err := v.driver.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
	if err != nil {
		return 0, err
	}

	elem, err := v.driver.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}

	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}

	vacancyCountText := strings.Fields(vacancyCountRaw)

	count, err := strconv.Atoi(vacancyCountText[1])
	if err != nil {
		return 0, err
	}

	return count, nil

}

func (v *VacancyPars) Vacancies(count int, query string) ([]string, error) {

	var links []string

	for i := 1; i <= count/PageVacancies+1; i++ {

		_ = v.driver.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", i, query))

		elems, err := v.driver.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return nil, err
		}
		for j := range elems {
			link, err := elems[j].GetAttribute("href")
			if err != nil {
				continue
			}
			links = append(links, HabrCareerLink+link)
		}
	}
	return links, nil
}

func (v *VacancyPars) JSONBySelector(link string) (entity.Vacancy, error) {
	resp, err := http.Get(link)
	if err != nil {
		log.Fatal(err)
		return entity.Vacancy{}, err
	}

	var doc *goquery.Document

	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatal(err)
		return entity.Vacancy{}, err
	}

	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		log.Println("habr vacancy nodes not found")
		return entity.Vacancy{}, errors.New("\nhabr vacancy nodes not found")
	}

	ss := dd.First().Text()

	var vacancy entity.Vacancy
	err = json.Unmarshal([]byte(ss), &vacancy)
	if err != nil {
		log.Fatal(err)
		return entity.Vacancy{}, err
	}
	return vacancy, nil
}
