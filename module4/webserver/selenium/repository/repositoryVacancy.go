package repository

import (
	"fmt"
	"strconv"
	"sync"

	"gitlab.com/konfka/go-go-kata/module4/webserver/selenium/internal/entity"
)

type VacancyStorage struct {
	primaryMap map[int]*entity.Vacancy
	sync.Mutex
}

func NewVacancyStorage() *VacancyStorage {
	return &VacancyStorage{
		primaryMap: make(map[int]*entity.Vacancy),
	}
}

func (v *VacancyStorage) Create(dto entity.Vacancy) error {
	v.Lock()
	defer v.Unlock()

	vacancyID, err := strconv.Atoi(dto.Identifier.Value)
	if err != nil {
		return err
	}

	v.primaryMap[vacancyID] = &dto
	return nil
}

func (v *VacancyStorage) GetByID(id int) (entity.Vacancy, error) {
	v.Lock()
	defer v.Unlock()

	val, ok := v.primaryMap[id]
	if ok {
		return *val, nil
	}
	return entity.Vacancy{}, fmt.Errorf("\nThere is no vacancy with this id")
}

func (v *VacancyStorage) GetList() ([]entity.Vacancy, error) {
	v.Lock()
	defer v.Unlock()

	result := make([]entity.Vacancy, 0, len(v.primaryMap))

	for _, val := range v.primaryMap {
		result = append(result, *val)
	}
	if result == nil {
		return nil, fmt.Errorf("\n There is no vacancies")
	}
	return result, nil
}

func (v *VacancyStorage) Delete(id int) error {
	v.Lock()
	defer v.Unlock()

	_, ok := v.primaryMap[id]
	if !ok {
		return fmt.Errorf("\nThere is no vacancy with this id")
	}

	delete(v.primaryMap, id)

	return nil
}
