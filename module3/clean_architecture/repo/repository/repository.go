package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"sort"

	"github.com/brianvoe/gofakeit/v6"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriter
	Map  map[int]interface{}
}

func NewUserRepository(file io.ReadWriter) *UserRepository {
	return &UserRepository{
		File: file,
		Map:  map[int]interface{}{},
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func checkErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	f, err := json.Marshal(record)
	checkErr(err)
	_, err = r.File.Write(f)
	checkErr(err)
	_, err = r.File.Write([]byte("\n"))
	if err != nil {
		return err
	}
	element, _ := record.(User)
	r.Map[element.ID] = record
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	val, ok := r.Map[id]
	if ok {
		return val, nil
	}
	return nil, errors.New("There is no element with this ID")
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	nums := make([]int, 0, len(r.Map))
	for i := range r.Map {
		nums = append(nums, i)
	}
	sort.Ints(nums)
	sliceOfRecords := []interface{}{}
	for _, v := range nums {
		sliceOfRecords = append(sliceOfRecords, r.Map[v])
	}
	return sliceOfRecords, nil
}

func GenerateJSON() []User {
	jsonFile := &[]User{}
	for i := 0; i <= 1000; i++ {
		*jsonFile = append(*jsonFile, User{
			ID:   i,
			Name: gofakeit.Name(),
		})
	}
	return *jsonFile
}

func main() {
	file, err := os.OpenFile("module3/clean_architecture/repo/test.json", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	checkErr(err)
	repo := NewUserRepository(file)
	//commits := GenerateJSON()

	/*for _, v := range commits {
		err = repo.Save(v)
		checkErr(err)
	}
	oh1 := &User{
		ID:   1001,
		Name: "Some Guy",
	}
	_ = repo.Save(*oh1)

	find, err := repo.Find(56)
	checkErr(err)
	fmt.Println(find)*/
	all, err := repo.FindAll()
	checkErr(err)
	_ = all
	nums := make([]int, 0, len(repo.Map))
	for i := range repo.Map {
		nums = append(nums, i)
	}
	sort.Ints(nums)
}
