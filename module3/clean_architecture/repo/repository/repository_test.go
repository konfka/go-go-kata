package main

import (
	"fmt"
	"os"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	file, err := os.OpenFile("../test.json", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	checkErr(err)
	rep := NewUserRepository(file)
	commits := GenerateJSON()
	want := 0
	for _, v := range commits {
		err = rep.Save(v)
		want++
		checkErr(err)
		if len(rep.Map) != want {
			t.Errorf("Expected list length: %d, but got: %d", want, len(rep.Map))
		}
	}
}

func TestUserRepository_Find(t *testing.T) {
	file, err := os.OpenFile("../test.json", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	checkErr(err)
	rep := NewUserRepository(file)
	commits := GenerateJSON()
	for _, v := range commits {
		err = rep.Save(v)
		checkErr(err)
	}
	want := rep.Map[56]
	actual, err := rep.Find(56)
	checkErr(err)
	if actual != want {
		t.Errorf("Expected element: %d, but got: %d", want, actual)
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	file, err := os.OpenFile("../test.json", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	checkErr(err)
	rep := NewUserRepository(file)
	commits := GenerateJSON()
	for _, v := range commits {
		err = rep.Save(v)
		checkErr(err)
	}
	var m = rep.Map
	fmt.Println(m[0])
	actual, err := rep.FindAll()
	checkErr(err)
	fmt.Println(actual[0])
	for i := range actual {
		if actual[i] != m[i] {
			t.Errorf("Expected element: %d, but got: %d", m[i], actual[i])
		}
	}
}
