package service

import (
	"fmt"
	time2 "time"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/model"
)

type TodoService struct {
	Service model.TodoRepository
}

func checkErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func (s *TodoService) ListTodos() ([]model.Todo, error) {
	todos, err := s.Service.GetTodos()
	checkErr(err)
	return todos, nil
}

func (s *TodoService) CreateTodo(name string) error {
	time := time2.Now()
	task := model.Todo{
		Name:   name,
		Time:   time,
		Status: false,
	}
	_, err := s.Service.CreateTodo(task)
	if err != nil {
		return err
	}
	return nil

}

func (s *TodoService) CompleteTodo(todo model.Todo) error {
	todo.Status = true

	err := s.Service.ChangeStatus(todo.ID, true)
	checkErr(err)
	return nil
}

func (s *TodoService) RemoveTodo(todo model.Todo) error {
	err := s.Service.DeleteTodo(todo.ID)
	if err != nil {
		return err
	}
	return nil
}
