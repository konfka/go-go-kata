package test

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"testing"
	"time"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/model"

	"github.com/stretchr/testify/assert"

	repository "gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/repo"
)

func checkErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func TestFileTaskRepository_GetTasks(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}

	jsonFile, err := os.Open(repo.FilePath)
	checkErr(err)
	byteValue, err := io.ReadAll(jsonFile)
	checkErr(err)
	var result repository.Tasks
	err = json.Unmarshal(byteValue, &result)
	checkErr(err)
	getTasks, err := repo.GetTasks()
	assert.NoError(t, err)
	assert.Equal(t, result, getTasks)

}

func TestFileTaskRepository_GetTask(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}
	task, _ := repo.GetTask(11)
	time := task.Time
	name := task.Name
	status := task.Status
	want := repository.Task{
		ID:     11,
		Name:   name,
		Time:   time,
		Status: status,
	}

	getTasks, err := repo.GetTask(11)
	assert.NoError(t, err)
	assert.Equal(t, want, getTasks)
}

func TestFileTaskRepository_CreateTask(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}
	want := repository.Task{
		ID:     1002,
		Name:   "Anything",
		Time:   time.Now(),
		Status: false,
	}

	getTasks, err := repo.CreateTask(want)
	assert.NoError(t, err)
	assert.Equal(t, want, getTasks)
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}
	want := repository.Task{
		ID:     1000,
		Name:   "Very Good",
		Time:   time.Now(),
		Status: false,
	}

	getTasks, err := repo.UpdateTask(want)
	assert.NoError(t, err)
	assert.Equal(t, want, getTasks)
}

func TestFileTaskRepository_DeleteTask(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}
	tasks, _ := repo.GetTasks()
	want := repository.Tasks{}
	for i, v := range tasks {
		if v.ID == 10 {
			mid := tasks[:i]
			want = append(mid, tasks[i+1:]...)
			break
		}
	}

	_ = repo.DeleteTask(10)
	actual, err := repo.GetTasks()
	assert.NoError(t, err)
	assert.Equal(t, want, actual)
}

func TestFileTaskRepository_GetTodo(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}
	tasks, _ := repo.GetTodos()
	want := model.Todo{}
	for _, v := range tasks {
		if v.ID == 11 {
			want = v
			break
		}
	}

	actual, err := repo.GetTodo(11)
	assert.NoError(t, err)
	assert.Equal(t, want, actual)
}

func TestFileTaskRepository_GetTodos(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}

	jsonFile, err := os.Open(repo.FilePath)
	checkErr(err)
	byteValue, err := io.ReadAll(jsonFile)
	checkErr(err)
	var result model.Todos
	err = json.Unmarshal(byteValue, &result)
	checkErr(err)
	getTasks, err := repo.GetTodos()
	assert.NoError(t, err)
	assert.Equal(t, result, getTasks)
}

func TestFileTaskRepository_ChangeStatus(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}
	todo, _ := repo.GetTodo(1000)
	want := model.Todo{
		ID:     1000,
		Name:   "Very Good",
		Time:   todo.Time,
		Status: false,
	}

	err := repo.ChangeStatus(1000, false)
	actual, _ := repo.GetTodo(1000)
	assert.NoError(t, err)
	assert.Equal(t, want, actual)
}

func TestFileTaskRepository_CreateTodo(t *testing.T) {
	repo := &repository.FileTaskRepository{FilePath: "../test.json"}
	want := model.Todo{
		ID:     10,
		Name:   "Some",
		Time:   time.Now(),
		Status: true,
	}

	actual, err := repo.CreateTodo(want)
	assert.NoError(t, err)
	assert.Equal(t, want, actual)
}

func TestFileTaskRepository_DeleteTodo(t *testing.T) {
	file, err := os.CreateTemp("", "todos.json")
	assert.NoError(t, err)

	todos := []model.Todo{
		{ID: 1, Name: "Todo 1", Status: true},
		{ID: 2, Name: "Todo 2", Status: true},
	}
	data, err := json.Marshal(todos)
	assert.NoError(t, err)
	err = os.WriteFile(file.Name(), data, 0644)
	assert.NoError(t, err)
	repo := &repository.FileTaskRepository{FilePath: file.Name()}

	err = repo.DeleteTodo(1)
	assert.NoError(t, err)

	data, err = os.ReadFile(file.Name())
	assert.NoError(t, err)
	var updatedTodos []model.Todo
	err = json.Unmarshal(data, &updatedTodos)
	assert.NoError(t, err)
	assert.Equal(t, todos[1], updatedTodos[0])
	err = os.Remove(file.Name())
	checkErr(err)
}
