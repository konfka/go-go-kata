package test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/model"
	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/service"
)

type testRepository struct {
	GetTodoFunc      func(id int) (model.Todo, error)
	GetTodosFunc     func() ([]model.Todo, error)
	CreateTodoFunc   func(todo model.Todo) (model.Todo, error)
	ChangeStatusFunc func(id int, status bool) error
	DeleteTodoFunc   func(id int) error
	UpdateTodosFunc  func(todos []model.Todo) error
}

func (r *testRepository) GetTodo(id int) (model.Todo, error) {
	if r.GetTodoFunc != nil {
		return r.GetTodoFunc(id)
	}
	return model.Todo{}, nil
}

func (r *testRepository) GetTodos() ([]model.Todo, error) {
	if r.GetTodosFunc != nil {
		return r.GetTodosFunc()
	}
	return nil, nil
}

func (r *testRepository) CreateTodo(todo model.Todo) (model.Todo, error) {
	if r.CreateTodoFunc != nil {
		return r.CreateTodoFunc(todo)
	}
	return model.Todo{}, nil
}

func (r *testRepository) ChangeStatus(id int, status bool) error {
	if r.ChangeStatusFunc(id, status) != nil {
		return r.ChangeStatusFunc(id, status)
	}
	return nil
}

func (r *testRepository) DeleteTodo(id int) error {
	if r.DeleteTodoFunc != nil {
		return r.DeleteTodoFunc(id)
	}
	return nil
}

func (r *testRepository) UpdateTodos(todos []model.Todo) error {
	if r.UpdateTodosFunc != nil {
		return r.UpdateTodosFunc(todos)
	}
	return nil
}

func (r *testRepository) SetGetTodosFunc(fn func() ([]model.Todo, error)) {
	r.GetTodosFunc = fn
}

func (r *testRepository) SetCreateTodoFunc(fn func(todo model.Todo) (model.Todo, error)) {
	r.CreateTodoFunc = fn
}

func (r *testRepository) SetChangeStatusFunc(fn func(id int, status bool) error) {
	r.ChangeStatusFunc = fn
}

func (r *testRepository) SetDeleteTodoFunc(fn func(id int) error) {
	r.DeleteTodoFunc = fn
}

func (r *testRepository) SetGetTodoFunc(fn func(id int) (model.Todo, error)) {
	r.GetTodoFunc = fn
}

func (r *testRepository) SetUpdateTodosFunc(fn func(todos []model.Todo) error) {
	r.UpdateTodosFunc = fn
}

func TestTodoService_ListTodos(t *testing.T) {
	repo := &testRepository{}

	todoSvc := &service.TodoService{
		Service: repo,
	}
	expected := []model.Todo{
		{ID: 1, Name: "Wash car", Time: time.Now(), Status: true},
		{ID: 2, Name: "Dinner with partner", Time: time.Now(), Status: false},
	}

	repo.GetTodosFunc = func() ([]model.Todo, error) {
		return expected, nil
	}

	todos, err := todoSvc.ListTodos()

	assert.NoError(t, err)
	assert.Equal(t, expected, todos)
}

func TestTodoService_CreateTodo(t *testing.T) {

	repo := &testRepository{}

	todoSvc := &service.TodoService{
		Service: repo,
	}

	repo.CreateTodoFunc = func(todo model.Todo) (model.Todo, error) {
		todo.ID = 1
		return todo, nil
	}

	err := todoSvc.CreateTodo("Wash car")

	assert.NoError(t, err)
}

func TestTodoService_CompleteTodo(t *testing.T) {
	repo := &testRepository{}

	todoSvc := &service.TodoService{
		Service: repo,
	}
	todo := model.Todo{ID: 1, Name: "Wash car", Time: time.Now(), Status: false}

	repo.ChangeStatusFunc = func(id int, status bool) error {

		assert.Equal(t, todo.ID, id)
		assert.Equal(t, true, status)
		return nil
	}

	err := todoSvc.CompleteTodo(todo)

	assert.NoError(t, err)
}
