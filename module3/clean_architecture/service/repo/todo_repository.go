package repository

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/model"
)

func checkErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

type Task struct {
	ID     int       `json:"id"`
	Name   string    `json:"name"`
	Time   time.Time `json:"time"`
	Status bool      `json:"status"`
}

type Tasks []Task

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks() (Tasks, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) (Task, error)
	UpdateTask(task Task) (Task, error)
	DeleteTask(id int) error
	WriteTasks(record Tasks) error
}

type FileTaskRepository struct {
	FilePath string
}

func (r *FileTaskRepository) WriteTasks(record Tasks) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	jsonFile, err := os.OpenFile(r.FilePath, os.O_APPEND|os.O_RDWR, 0644)
	checkErr(err)
	byteValue, _ := io.ReadAll(jsonFile)
	var existing Tasks
	_ = json.Unmarshal(byteValue, &existing)
	existing = append(existing, record...)
	f, err := json.Marshal(existing)
	checkErr(err)
	_, err = jsonFile.Seek(0, io.SeekStart)
	checkErr(err)
	_ = jsonFile.Truncate(0)
	_, err = jsonFile.Write(f)
	checkErr(err)
	_ = jsonFile.Close()
	return nil
}

// GetTasks returns all tasks from the repository
func (r *FileTaskRepository) GetTasks() (Tasks, error) {
	jsonFile, err := os.Open(r.FilePath)
	checkErr(err)
	byteValue, err := io.ReadAll(jsonFile)
	checkErr(err)
	var result Tasks
	err = json.Unmarshal(byteValue, &result)
	checkErr(err)
	_ = jsonFile.Close()
	return result, nil
}

// GetTask returns a single task by its ID
func (r *FileTaskRepository) GetTask(id int) (Task, error) {

	tasks, err := r.GetTasks()
	checkErr(err)

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return Task{}, errors.New("There is no task with this ID! ")
}

// CreateTask adds a new task to the repository
func (r *FileTaskRepository) CreateTask(task Task) (Task, error) {
	tasks, err := r.GetTasks()
	checkErr(err)
	for i, v := range tasks {
		if i != v.ID {
			task.ID = i
			break
		} else {
			task.ID = len(tasks) + 1
		}
	}
	var t = Tasks{task}
	err = r.WriteTasks(t)
	checkErr(err)

	return task, nil
}

// UpdateTask updates an existing task in the repository
func (r *FileTaskRepository) UpdateTask(task Task) (Task, error) {
	tasks, err := r.GetTasks()
	checkErr(err)

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	jsonFile, err := os.OpenFile(r.FilePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	checkErr(err)
	f, err := json.Marshal(tasks)
	checkErr(err)
	_, err = jsonFile.Seek(0, io.SeekStart)
	checkErr(err)
	_ = jsonFile.Truncate(0)
	_, err = jsonFile.Write(f)
	checkErr(err)
	_ = jsonFile.Close()

	checkErr(err)

	return task, nil
}

func (r FileTaskRepository) DeleteTask(id int) error {
	tasks, err := r.GetTasks()
	checkErr(err)
	var res Tasks
	for i, t := range tasks {
		if t.ID == id {
			mid := tasks[0:i]
			res = append(mid, tasks[i+1:]...)
			break
		}
	}

	jsonFile, err := os.OpenFile(r.FilePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	checkErr(err)
	f, err := json.Marshal(res)
	checkErr(err)
	_, err = jsonFile.Seek(0, io.SeekStart)
	checkErr(err)
	_ = jsonFile.Truncate(0)
	_, err = jsonFile.Write(f)
	checkErr(err)
	_ = jsonFile.Close()

	checkErr(err)
	return nil
}

func (r FileTaskRepository) GetTodo(id int) (model.Todo, error) {
	tasks, err := r.GetTasks()
	checkErr(err)

	for _, t := range tasks {
		if t.ID == id {
			res := model.Todo{
				ID:     t.ID,
				Name:   t.Name,
				Time:   t.Time,
				Status: t.Status,
			}
			return res, nil
		}
	}

	return model.Todo{}, errors.New("There is no todo with this ID! ")
}

func (r *FileTaskRepository) GetTodos() (model.Todos, error) {
	jsonFile, err := os.Open(r.FilePath)
	checkErr(err)
	byteValue, err := io.ReadAll(jsonFile)
	checkErr(err)
	var result model.Todos
	err = json.Unmarshal(byteValue, &result)
	checkErr(err)
	_ = jsonFile.Close()
	return result, nil
}

func (r *FileTaskRepository) ChangeStatus(id int, status bool) error {
	tasks, err := r.GetTodos()
	checkErr(err)

	for i, t := range tasks {
		if t.ID == id {
			tasks[i].Status = status
			break
		}
	}

	jsonFile, err := os.OpenFile(r.FilePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	checkErr(err)
	f, err := json.Marshal(tasks)
	checkErr(err)
	_, err = jsonFile.Seek(0, io.SeekStart)
	checkErr(err)
	_ = jsonFile.Truncate(0)
	_, err = jsonFile.Write(f)
	checkErr(err)
	_ = jsonFile.Close()

	checkErr(err)
	return nil
}

func (r *FileTaskRepository) CreateTodo(todo model.Todo) (model.Todo, error) {
	tasks, err := r.GetTodos()
	checkErr(err)

	task := Task{
		ID:     0,
		Name:   todo.Name,
		Time:   todo.Time,
		Status: todo.Status,
	}

	for i, v := range tasks {
		if i != v.ID {
			task.ID = i
			break
		} else {
			task.ID = len(tasks) + 1
		}
	}

	_, err = r.CreateTask(task)
	checkErr(err)

	createTodo := model.Todo{
		ID:     task.ID,
		Name:   task.Name,
		Time:   task.Time,
		Status: task.Status,
	}
	return createTodo, nil
}

func (r FileTaskRepository) DeleteTodo(id int) error {
	err := r.DeleteTask(id)
	if err != nil {
		return err
	}
	return nil
}
