package main

import (
	"fmt"

	repository "gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/repo"

	"github.com/brianvoe/gofakeit/v6"
)

func checkErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func GenerateJSON() []repository.Task {
	jsonFile := &[]repository.Task{}
	for i := 0; i <= 1000; i++ {
		*jsonFile = append(*jsonFile, repository.Task{
			ID:     i,
			Name:   gofakeit.VerbAction(),
			Time:   gofakeit.Date(),
			Status: gofakeit.Bool(),
		})
	}
	return *jsonFile
}

func main() {
	repo := &repository.FileTaskRepository{FilePath: "module3/clean_architecture/service/test.json"}
	//time := time.Now()
	tasks := GenerateJSON()
	err := repo.WriteTasks(tasks)
	checkErr(err)
	/*taskList, err := repo.GetTasks()
	checkErr(err)
	fmt.Println(taskList)
	fmt.Println(repo.GetTask(675))*/
	/*tasks := repository.Task{
		ID:     1,
		Name:   "Gop",
		Time:   time,
		Status: true,
	}
	//_, _ = repo.UpdateTask(tasks)*/

}
