package delivery

import (
	"errors"
	"fmt"
	"os"
	time2 "time"

	model2 "gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/model"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/cli/model"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/cli/service"
)

type Delivery struct {
	Deliver service.TodoService
}

func New(n service.TodoService) Delivery {
	return Delivery{Deliver: n}
}

func (d *Delivery) Run() error {
	for {
		fmt.Print(
			"Выберите действие:\n" +
				"==================================\n" +
				"1. Показать список задач;\n" +
				"2. Добавить задачу;\n" +
				"3. Удалить задачу;\n" +
				"4. Редактировать задачу по ID;\n" +
				"5. Завершить задачу;\n" +
				"6. Выйти из программы.\n" +
				"==================================\n" +
				"Введите число: ")

		var do int
		_, err := fmt.Scanln(&do)
		if err != nil {
			fmt.Println(err)
		}
		switch do {
		case 1:
			err = d.GetAllTasks()
			if err != nil {
				fmt.Println(err)
			}
		case 2:
			err = d.AddTask()
			if err != nil {
				fmt.Println(err)
			}
		case 3:
			err = d.DeleteTask()
			if err != nil {
				fmt.Println(err)
			}
		case 4:
			err = d.EditTask()
			if err != nil {
				fmt.Println(err)
			}
		case 5:
			err = d.TerminateTask()
			if err != nil {
				fmt.Println(err)
			}
		case 6:
			os.Exit(0)
			return nil
		}
	}
}

func (d *Delivery) GetAllTasks() error {
	tasks, err := d.Deliver.ListTodos()

	for _, v := range tasks {
		fmt.Println(v)
	}
	return err
}

func (d *Delivery) AddTask() error {
	var name string
	fmt.Print("Напиши название задачи: ")
	_, err := fmt.Scanln(&name)
	if err != nil {
		fmt.Println(err)
	}
	_, err = d.Deliver.CreateTodo(name)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Задача создана!")
	return err
}

func (d *Delivery) DeleteTask() error {
	tasks, _ := d.Deliver.Service.GetTodos()
	var id int
	fmt.Println("Напишите ID задачи к удалению: ")
	_, err := fmt.Scanln(&id)
	if err != nil {
		fmt.Println(err)
	}
	if id > len(tasks) || id < 0 {
		return errors.New("Задачи с таким ID не существует. ")
	}
	err = d.Deliver.RemoveTodo(id)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Задача была удалена!")
	return err
}

func (d *Delivery) EditTask() error {
	allTasks, _ := d.Deliver.Service.GetTodos()
	var id int
	var name string
	var time = time2.Now()
	fmt.Println("Введите ID задачи: ")
	_, err := fmt.Scanln(&id)
	if err != nil {
		return err
	}
	if id > len(allTasks) || id < 0 {
		return errors.New("Задачи с таким ID не существует. ")
	}
	fmt.Println("Введите новое название задачи: ")
	_, err = fmt.Scanln(&name)
	if err != nil {
		return err
	}
	var task = model2.Todo{ID: id, Time: time, Name: name}
	var tasks = []model2.Todo{{
		ID:   id,
		Name: name,
		Time: time,
	},
	}
	tasks[0] = task
	err = d.Deliver.UpdateTodos(tasks)
	if err != nil {
		fmt.Println(err)
	}
	return err
}

func (d *Delivery) TerminateTask() error {
	allTasks, _ := d.Deliver.Service.GetTodos()
	var id int
	fmt.Println("Введите ID задачи: ")
	_, err := fmt.Scanln(&id)
	if err != nil {
		return err
	}
	if id > len(allTasks) || id < 0 {
		return errors.New("Задачи с таким ID не существует. ")
	}
	task := model.Todo{ID: id}
	err = d.Deliver.CompleteTodo(model2.Todo(task))
	if err != nil {
		fmt.Println(err)
	}
	return err
}
