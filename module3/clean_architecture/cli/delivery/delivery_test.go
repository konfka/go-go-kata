package delivery

import (
	"testing"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/cli/service"
)

func TestDelivery_GetAllTasks(t *testing.T) {
	type fields struct {
		deliver service.TodoService
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Delivery{
				Deliver: tt.fields.deliver,
			}
			if err := d.GetAllTasks(); (err != nil) != tt.wantErr {
				t.Errorf("GetAllTasks() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
