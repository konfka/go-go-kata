package service

import (
	"fmt"
	time2 "time"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/service/model"
)

type TodoService struct {
	Service model.TodoRepository
}

func New(repo model.TodoRepository) *TodoService {
	return &TodoService{repo}
}

func checkErr(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func (s *TodoService) ListTodos() ([]model.Todo, error) {
	todos, err := s.Service.GetTodos()
	checkErr(err)
	return todos, nil
}

func (s *TodoService) CreateTodo(name string) (int, error) {
	time := time2.Now()
	task := model.Todo{
		Name:   name,
		Time:   time,
		Status: false,
	}
	todo, err := s.Service.CreateTodo(task)
	if err != nil {
		return todo.ID, err
	}
	return todo.ID, nil

}

func (s *TodoService) CompleteTodo(todo model.Todo) error {
	todo.Status = true

	err := s.Service.ChangeStatus(todo.ID, true)
	checkErr(err)
	return nil
}

func (s *TodoService) RemoveTodo(id int) error {
	err := s.Service.DeleteTodo(id)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) UpdateTodos(todos []model.Todo) error {
	err := s.Service.UpdateTodos(todos)
	return err
}
