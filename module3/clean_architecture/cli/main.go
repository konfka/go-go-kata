package main

import (
	"fmt"

	"github.com/brianvoe/gofakeit/v6"

	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/cli/delivery"
	repository "gitlab.com/konfka/go-go-kata/module3/clean_architecture/cli/repo"
	"gitlab.com/konfka/go-go-kata/module3/clean_architecture/cli/service"
)

func GenerateJSON() []repository.Task {
	jsonFile := &[]repository.Task{}
	for i := 0; i <= 1000; i++ {
		*jsonFile = append(*jsonFile, repository.Task{
			ID:     i,
			Name:   gofakeit.VerbAction(),
			Time:   gofakeit.Date(),
			Status: gofakeit.Bool(),
		})
	}
	return *jsonFile
}

func main() {
	repo := repository.New("module3/clean_architecture/cli/test.json")
	serve := service.New(repo)
	deliver := delivery.New(*serve)

	/*tasks := GenerateJSON()
	err := repo.WriteTasks(tasks)
	if err != nil {
		fmt.Println(err)
	}*/
	err := deliver.Run()
	if err != nil {
		fmt.Println(err)
	}
}
