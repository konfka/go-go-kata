package model

import (
	"time"
)

type Todo struct {
	ID     int       `json:"id"`
	Name   string    `json:"name"`
	Time   time.Time `json:"time"`
	Status bool      `json:"status"`
}

type TodoService interface {
	ListTodos() ([]Todo, error)
	CreateTodo(name string) error
	CompleteTodo(todo Todo) error
	RemoveTodo(id int) error
	UpdateTodos(todos []Todo) error
}

type TodoRepository interface {
	GetTodo(id int) (Todo, error)
	GetTodos() ([]Todo, error)
	CreateTodo(todo Todo) (Todo, error)
	ChangeStatus(id int, status bool) error
	DeleteTodo(id int) error
	UpdateTodos(todos []Todo) error
}
