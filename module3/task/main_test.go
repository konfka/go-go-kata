package main

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var tim = time.Now()

var com1 = &Commit{
	Message: "Hello, what's up!",
	UUID:    "1",
	Date:    tim,
}
var com2 = &Commit{
	Message: "Goodbye",
	UUID:    "2",
	Date:    tim,
}
var com3 = &Commit{
	Message: "Where you at?",
	UUID:    "3",
	Date:    tim,
}

var node1 = &Node{data: com1}
var node2 = &Node{data: com2}
var node3 = &Node{data: com3}

var d = &DoubleLinkedList{}

func TestDoubleLinkedList_LoadData(t *testing.T) {

	err := d.LoadData("./test.json")
	if err != nil {
		t.Errorf("Failed to load data: %v", err)
	}

	want := 2000
	if d.Len() != want {
		t.Errorf("Expected list length: %d, but got: %d", want, d.Len())
	}
}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {

	for i := 0; i < b.N; i++ {
		err := d.LoadData("./test.json")
		if err != nil {
			b.Errorf("Failed to load data: %v", err)
		}
	}

}

func TestDoubleLinkedList_Len(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.tail = node3
	d.len = 3

	type args struct {
		lists *DoubleLinkedList
	}
	tests := []struct {
		len  string
		args args
		want int
	}{
		{
			len:  "Len Method",
			args: args{lists: d},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.len, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.args.lists.Len(), "Length(%v)", tt.args.lists)
		})
	}
}

func BenchmarkDoubleLinkedList_Len(b *testing.B) {

	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d.Len()
	}

}

func TestDoubleLinkedList_Current(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Current Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  3,
			},
			want: d.curr,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, d.Current(), "Current()")
		})
	}
}

func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d.Current()
	}

}

func TestDoubleLinkedList_Next(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Next Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  3,
			},
			want: d.curr.next,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, d.Next(), "Next()")
		})
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d.Next()
	}

}

func TestDoubleLinkedList_Prev(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Prev Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  3,
			},
			want: d.curr.prev,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, d.Prev(), "Prev()")
		})
	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d.Prev()
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
		c *Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr error
	}{
		{
			name: "Insert",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  d.len,
			},
			args: args{
				n: 2,
				c: &Commit{
					Message: "Oh yessssss",
					UUID:    "228",
					Date:    tim,
				},
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.wantErr, d.Insert(tt.args.n, tt.args.c), fmt.Sprintf("Insert(%v, %v)", tt.args.n, tt.args.c))
		})
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	for i := 0; i < b.N; i++ {
		err := d.Insert(0, com1)
		if err != nil {
			b.Errorf("Invalid insert commit: %v", err)
		}
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr error
	}{
		{
			name: "Delete Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  3,
			},
			args:    args{n: 1},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.wantErr, d.Delete(tt.args.n), fmt.Sprintf("Delete(%v)", tt.args.n))
		})
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}

	b.ResetTimer()

	for i := 0; i < 1000; i++ {
		_ = d.Delete(1)
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr error
	}{
		{
			name: "DeleteCurr Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  3,
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.wantErr, d.DeleteCurrent(), "DeleteCurrent()")
		})
	}
}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}
	d.curr = d.tail
	b.ResetTimer()

	for i := 1; i < 2000; i++ {
		_ = d.DeleteCurrent()
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr error
	}{
		{
			name: "Index Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  3,
			},
			want:    1,
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.Index()
			if err != nil {
				return
			}
			assert.Equalf(t, tt.want, got, "Index()")
		})
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}
	d.curr = d.tail
	b.ResetTimer()
	for i := 1; i < 2000; i++ {
		_, _ = d.Index()
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Pop Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  d.Len(),
			},
			want: node3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, v.Pop(), "Pop()")
		})
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}
	d.curr = d.tail
	b.ResetTimer()

	for i := 1; i < 2000; i++ {
		_ = d.Pop()
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "Shift Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  d.Len(),
			},
			want: node1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, d.Shift(), "Shift()")
		})
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}
	d.curr = d.tail
	b.ResetTimer()
	for i := 1; i < 2000; i++ {
		_ = d.Shift()
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		uuID string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name: "SearchUUID Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  d.Len(),
			},
			args: args{uuID: "2"},
			want: node2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, d.SearchUUID(tt.args.uuID), "SearchUUID(%v)", tt.args.uuID)
		})
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}
	d.curr = d.tail
	b.ResetTimer()
	for i := 1; i < b.N; i++ {
		d.SearchUUID("6957bd28-875b-11ed-8150-acde48001122")
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		message string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name: "Search Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  d.Len(),
			},
			args: args{message: "Goodbye"},
			want: node2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, v.Search(tt.args.message), "Search(%v)", tt.args.message)
		})
	}
}

func BenchmarkDoubleLinkedList_Search(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}
	d.curr = d.tail
	b.ResetTimer()
	for i := 1; i < b.N; i++ {
		d.Search("You can't hack the pixel without calculating the auxiliary THX pixel!")
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {

	b := &DoubleLinkedList{}
	var node1b = &Node{data: com1}
	var node2b = &Node{data: com2}
	var node3b = &Node{data: com3}

	d.head = node1
	d.head.next = node2
	node2.prev = d.head
	node2.next = node3
	node3.prev = node2
	d.curr = node2
	d.curr.next = node3
	d.curr.prev = node1
	d.tail = node3
	d.len = 3

	b.head = node3b
	b.head.next = node2b
	node2b.prev = b.head
	node2b.next = node1b
	node1b.prev = node2b
	b.curr = node2b
	b.curr.next = node1b
	b.curr.prev = node3b
	b.tail = node1b
	b.len = 3

	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	tests := []struct {
		name   string
		fields fields
		want   *DoubleLinkedList
	}{
		{
			name: "Reverse Method",
			fields: fields{
				head: d.head,
				tail: d.tail,
				curr: d.curr,
				len:  d.Len(),
			},
			want: b,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			assert.Equalf(t, tt.want, d.Reverse(), "Reverse()")
		})
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	err := d.LoadData("./test.json")
	if err != nil {
		b.Errorf("Failed to load data: %v", err)
	}
	d.curr = d.tail
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d.Reverse()
	}
}
