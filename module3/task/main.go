package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	// отсортировать список используя самописный QuickSort
	data := new([]Commit)
	body, err := os.Open(path)
	if err != nil {
		return err
	}
	jsonParser := json.NewDecoder(body)
	err = jsonParser.Decode(data)
	if err != nil {
		return err
	}
	newData := QuickSort(data)
	newNode := &Node{
		data: nil,
	}
	for _, v := range newData {
		value := v
		newNode = &Node{
			data: &value,
		}
		if d.head == nil {
			d.head = newNode
			d.curr = newNode
			d.curr.next = nil
			d.tail = newNode
		} else {
			currentNode := d.head
			for currentNode.next != nil {
				currentNode = currentNode.next
			}
			newNode.prev = currentNode
			currentNode.next = newNode
			d.tail = newNode
			d.curr = currentNode
		}
		d.len++
	}
	//d.tail.next = d.head
	//d.head.prev = d.tail
	return err
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	return d.curr.next
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c *Commit) error {
	node := &Node{data: c}
	if n < 0 || n > d.len {
		return errors.New("Invalid position")
	}
	if n == 0 && d.head == nil {
		d.head = node
		d.curr = node
		d.tail = node
		d.len++
		return nil
	}
	if n == 0 {
		node.next = d.head
		d.head = node
		d.curr = node
		d.len++
		return nil
	}
	if n == d.len {
		node.next = d.tail.next
		d.tail.next = node
		node.prev = d.tail
		d.len++
		return nil
	}
	currentNode := d.head
	for i := 0; i != n; i++ {
		/*if i == 1 {
			node.next = d.head.next
			d.head.next = node
			break
		}*/
		currentNode = currentNode.next
	}
	currentNode = currentNode.prev
	node.prev = currentNode
	node.next = currentNode.next
	currentNode.next = node
	currentNode = node
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {

	if d.head == nil {
		return errors.New("List is empty")
	}
	if n == 0 {
		d.head = d.head.next
		d.len--
		return nil
	}

	currentNode := d.head

	for i := 1; i < n; i++ {
		currentNode = currentNode.next
	}
	actual := currentNode.next
	prev := currentNode.prev
	next := actual.next

	currentNode.prev = prev
	currentNode.next = next
	currentNode = actual
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	currentInd, _ := d.Index()
	if d.head == nil {
		return errors.New("List is empty")
	}
	if currentInd == 0 {
		d.head = d.head.next
	}
	currentNode := d.head
	for i := 0; i != currentInd; i++ {
		currentNode = currentNode.next
	}
	actual := currentNode.next
	prev := currentNode.prev
	next := actual.next

	currentNode.prev = prev
	currentNode.next = next
	currentNode = actual
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	currentNode := d.head
	i := 0
	for currentNode != nil {
		if currentNode == d.curr {
			return i, nil
		}
		i++
		currentNode = currentNode.next
	}
	return 0, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if (*d).tail == nil {
		return nil
	}
	res := d.tail
	d.tail = d.tail.prev
	d.tail.next = nil
	d.len--
	if d.tail == nil {
		d.head = nil
	}
	return res
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if (*d).tail == nil {
		return nil
	}
	res := d.head
	d.head = d.head.next
	d.head.prev = nil
	d.len--
	return res
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	currentNode := d.head
	for currentNode.next != nil {
		if currentNode.data.UUID == uuID {
			return currentNode
		}
		currentNode = currentNode.next
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	currentNode := d.head
	for currentNode.next != nil {
		if currentNode.data.Message == message {
			return currentNode
		}
		currentNode = currentNode.next
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	//newList := &DoubleLinkedList{}
	if d.head == nil {
		return nil
	}
	currentNode := d.head
	var prevNode *Node = nil

	for currentNode != nil {
		nextNode := currentNode.next
		currentNode.next = prevNode
		currentNode.prev = nextNode
		prevNode = currentNode
		currentNode = nextNode
	}

	d.head, d.tail = d.tail, d.head

	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON() {
	jsonFile := &[]Commit{}
	for i := 0; i < 1000; i++ {
		*jsonFile = append(*jsonFile, Commit{
			Message: gofakeit.Phrase(),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		})
	}
	for _, v := range *jsonFile {
		fmt.Println(v)
	}
}

func (d *DoubleLinkedList) PrintList() {
	currentNode := d.head
	for currentNode != nil {
		fmt.Println(currentNode.data)
		currentNode = currentNode.next
	}
}

func QuickSort(arr *[]Commit) []Commit {
	if len(*arr) <= 1 {
		return *arr
	}

	median := (*arr)[rand.Intn(len(*arr))].Date

	low_part := make([]Commit, 0, len(*arr))
	high_part := make([]Commit, 0, len(*arr))
	middle_part := make([]Commit, 0, len(*arr))

	for _, item := range *arr {
		switch {
		case item.Date.Before(median):
			low_part = append(low_part, item)
		case item.Date.After(median):
			high_part = append(high_part, item)
		case item.Date == median:
			middle_part = append(middle_part, item)
		}
	}

	low_part = QuickSort(&low_part)
	high_part = QuickSort(&high_part)

	low_part = append(low_part, middle_part...)
	low_part = append(low_part, high_part...)

	return low_part
}

func main() {
	/*t := time.Now()
	com := &Commit{
		Message: "Hello, what's up!",
		UUID:    "69582394-875b-11ed-8150-acde48001122",
		Date:    t,
	}*/
	d := new(DoubleLinkedList)
	_ = d.LoadData("module3/task/test.json")
	d.PrintList()
	fmt.Println(d.Len())
	fmt.Println(d.Index())
	fmt.Println(d.curr.data)
	GenerateJSON()
}
