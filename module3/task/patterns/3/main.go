package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

func checkErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

type weatherNow struct {
	Name string `json:"name"`
	Main struct {
		Temperature float64 `json:"temp"`
		Humidity    int     `json:"humidity"`
	} `json:"main"`
	Wind struct {
		WindSpeed float64 `json:"speed"`
	} `json:"wind"`
}

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

const BaseURL = "https://api.openweathermap.org/data/2.5/weather?"

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

func (o *OpenWeatherAPI) GetWeather(location string) (int, int, int) {

	URL := fmt.Sprintf("%sq=%s&appid=%s&units=metric", BaseURL, location, o.apiKey)

	resp, err := http.Get(URL)
	checkErr(err)

	defer func(Body io.ReadCloser) {
		err = Body.Close()
		checkErr(err)

	}(resp.Body)

	if resp.StatusCode != http.StatusOK {
		log.Fatal("unexpected response status", resp.Status)
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	checkErr(err)

	var weatherJson weatherNow
	err = json.Unmarshal(bodyBytes, &weatherJson)
	checkErr(err)

	temperature := weatherJson.Main.Temperature
	humidity := weatherJson.Main.Humidity
	windSpeed := weatherJson.Wind.WindSpeed

	return int(temperature), humidity, int(windSpeed)
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	temp, _, _ := o.GetWeather(location)
	return temp
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	_, humidity, _ := o.GetWeather(location)
	return humidity
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	_, _, windSpeed := o.GetWeather(location)
	return windSpeed
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {

	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("29e72cbf885d8f5a6249f3bc598b089d")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d °C\n", temperature)
		fmt.Printf("Humidity in "+city+": %d%\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d m/s \n\n", windSpeed)
	}
}
