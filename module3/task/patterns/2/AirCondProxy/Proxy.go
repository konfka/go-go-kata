package Proxy

import (
	"fmt"

	Adapter "gitlab.com/konfka/go-go-kata/module3/task/patterns/2/AirConditionerAdapter"
)

type AirConditionerProxy struct {
	adapter       *Adapter.AirConditionerAdapter
	authenticated bool
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		adapter:       Adapter.NewAirCondAdapter(),
		authenticated: authenticated}
}

func (proxy AirConditionerProxy) TurnOn() {
	if proxy.authenticated {
		proxy.adapter.TurnOn()
	} else {
		fmt.Println("В доступе отказано: чтобы включить кондиционер нужно быть авторизованым.")
	}
}

func (proxy AirConditionerProxy) TurnOff() {
	if proxy.authenticated {
		proxy.adapter.TurnOff()
	} else {
		fmt.Println("В доступе отказано: чтобы выключить кондиционер нужно быть авторизованым.")
	}
}

func (proxy AirConditionerProxy) SetTemperature(temp int) {
	if proxy.authenticated {
		proxy.adapter.SetTemperature(temp)
	} else {
		fmt.Println("В доступе отказано: чтобы изменить температуру нужно быть авторизованым.")
	}
}
