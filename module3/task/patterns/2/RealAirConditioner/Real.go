package Real

import "fmt"

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct{}

func NewRealAir() *RealAirConditioner {
	return &RealAirConditioner{}
}

func (r RealAirConditioner) TurnOn() {
	fmt.Println("Кондиционер включен.")
}

func (r RealAirConditioner) TurnOff() {
	fmt.Println("Кондиционер выключен.")
}

func (r RealAirConditioner) SetTemperature(temp int) {
	fmt.Printf("Выставленная температура: %d", temp)
}
