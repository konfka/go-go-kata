package Adapter

import (
	Real "gitlab.com/konfka/go-go-kata/module3/task/patterns/2/RealAirConditioner"
)

type AirConditionerAdapter struct {
	airConditioner *Real.RealAirConditioner
}

func NewAirCondAdapter() *AirConditionerAdapter {
	return &AirConditionerAdapter{
		airConditioner: Real.NewRealAir(),
	}
}

func (a AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}

func (a AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}

func (a AirConditionerAdapter) SetTemperature(temp int) {
	a.airConditioner.SetTemperature(temp)
}
