package main

import Proxy "gitlab.com/konfka/go-go-kata/module3/task/patterns/2/AirCondProxy"

func main() {
	airConditioner := Proxy.NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = Proxy.NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
