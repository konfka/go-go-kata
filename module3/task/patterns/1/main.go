package main

import (
	"fmt"

	Pricing "gitlab.com/konfka/go-go-kata/module3/task/patterns/1/PricingStrategy"
	Regular "gitlab.com/konfka/go-go-kata/module3/task/patterns/1/RegularPricing"
	Sale "gitlab.com/konfka/go-go-kata/module3/task/patterns/1/SalePricing"
)

func main() {
	product := Pricing.Order{
		Name:   "Хлеб",
		Price:  45,
		Amount: 4,
	}

	events := []string{"Весенняя скидка", "Скидка регулярного клиента", "Черная пятница", "Обычная цена"}

	prices := []Pricing.PricingStrategy{
		&Sale.SalePricing{},
		&Sale.SalePricing{},
		&Sale.SalePricing{},
		&Regular.RegularPricing{},
	}
	for i, v := range prices {
		num, err := v.Calculate(product, events[i])
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Printf("Обычная цена товара %s в количестве %d штук - %.2f, "+
				"Окончательная цена товара - %.2f\n", product.Name, product.Amount, product.Price*float64(product.Amount), num)
		}
	}
}
