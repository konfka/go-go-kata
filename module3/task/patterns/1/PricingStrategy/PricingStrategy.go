package Pricing

type PricingStrategy interface {
	Calculate(order Order, event string) (float64, error)
}

type Order struct {
	Name   string
	Price  float64
	Amount int
}
