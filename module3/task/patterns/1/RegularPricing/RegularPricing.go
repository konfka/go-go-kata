package Regular

import Pricing "gitlab.com/konfka/go-go-kata/module3/task/patterns/1/PricingStrategy"

type RegularPricing struct {
	Regular Pricing.PricingStrategy
}

var m = map[string]float64{"Обычная цена": 1.0}

func (s *RegularPricing) Calculate(order Pricing.Order, event string) (float64, error) {
	return order.Price * float64(order.Amount) * m[event], nil
}
