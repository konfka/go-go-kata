package Sale

import (
	"errors"

	Pricing "gitlab.com/konfka/go-go-kata/module3/task/patterns/1/PricingStrategy"
)

var discounts = map[string]float64{"Весенняя скидка": 0.2, "Скидка регулярного клиента": 0.05, "Черная пятница": 0.57}

type SalePricing struct {
	Regular Pricing.PricingStrategy
}

func (s *SalePricing) Calculate(o Pricing.Order, event string) (float64, error) {
	val, ok := discounts[event]
	if ok {
		return (o.Price - o.Price*val) * float64(o.Amount), nil
	}
	return 0, errors.New("Скидки по данному мероприятию не существует! ")
}
