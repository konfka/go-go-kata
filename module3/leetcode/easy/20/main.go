package main

import (
	"fmt"
	"sort"
)

func kidsWithCandies(candies []int, extraCandies int) []bool {
	modifiedCandies := []int{}
	modifiedCandies = append(modifiedCandies, candies...)
	sort.Ints(modifiedCandies)
	list := []bool{}
	max := modifiedCandies[len(modifiedCandies)-1]
	for _, v := range candies {
		if max > v+extraCandies {
			list = append(list, false)
		} else {
			list = append(list, true)
		}
	}
	return list
}

func main() {
	candies := []int{2, 3, 5, 1, 3}
	extraCandies := 3
	fmt.Println(kidsWithCandies(candies, extraCandies))
}
