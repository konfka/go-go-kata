package main

import "fmt"

func shuffle(nums []int, n int) []int {
	firstPrt := nums[:len(nums)/2]
	secondPrt := nums[len(nums)/2:]
	var merged []int
	for i := 0; i < len(firstPrt); i++ {
		merged = append(merged, firstPrt[i])
		merged = append(merged, secondPrt[i])
	}
	return merged
}

func main() {
	nums := []int{2, 5, 1, 3, 4, 7}
	n := 3
	fmt.Println(shuffle(nums, n))
}
