package main

import "fmt"

func createTargetArray(nums []int, index []int) []int {
	res := []int{}
	for i := range nums {
		if len(res) == index[i] {
			res = append(res, nums[i])
		} else {
			res = append(res[:index[i]+1], res[index[i]:]...)
			res[index[i]] = nums[i]
		}
	}
	return res
}

func main() {
	nums := []int{0, 1, 2, 3, 4}
	index := []int{0, 1, 2, 2, 1}
	fmt.Println(createTargetArray(nums, index))
}
