package main

import "fmt"

func countDigits(num int) int {
	counter := 0
	n := num
	for n > 0 {
		if num%(n%10) == 0 {
			counter++
		}
		n /= 10
	}
	return counter
}

func main() {
	fmt.Println(countDigits(7))
}
