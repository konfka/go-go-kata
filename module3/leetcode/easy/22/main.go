package main

import "fmt"

func smallerNumbersThanCurrent(nums []int) []int {
	res := []int{}
	for _, val := range nums {
		i := 0
		for _, v := range nums {
			if v < val {
				i++
			}
		}
		res = append(res, i)
	}
	return res
}

func main() {
	nums := []int{8, 1, 2, 2, 3}
	fmt.Println(smallerNumbersThanCurrent(nums))
}
