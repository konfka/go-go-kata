package main

import "fmt"

func decode(encoded []int, first int) []int {
	origin := []int{first}
	for _, v := range encoded {
		origin = append(origin, origin[len(origin)-1]^v)
	}
	return origin
}

func main() {
	encoded := []int{1, 2, 3}
	first := 1
	fmt.Println(decode(encoded, first))
}
