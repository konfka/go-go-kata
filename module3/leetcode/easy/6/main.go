package main

import (
	"fmt"
	"strings"
)

func uniqueMorseRepresentations(words []string) int {
	dict := map[string]string{"a": ".-", "b": "-...", "c": "-.-.", "d": "-..", "e": ".", "f": "..-.", "g": "--.", "h": "....", "i": "..", "j": ".---", "k": "-.-", "l": ".-..", "m": "--", "n": "-.", "o": "---", "p": ".--.", "q": "--.-", "r": ".-.", "s": "...", "t": "-", "u": "..-", "v": "...-", "w": ".--", "x": "-..-", "y": "-.--", "z": "--.."}
	convertedWords := []string{}
	for _, val := range words {
		convertWord := strings.Split(val, "")
		for j := 0; j < len(convertWord); j++ {
			convertWord[j] = dict[convertWord[j]]
		}
		convertedWords = append(convertedWords, strings.Join(convertWord, ""))
	}
	mapp := make(map[string]int)
	for _, v := range convertedWords {
		mapp[v] = 1
	}
	return len(mapp)
}

func main() {
	words := []string{"gin", "zen", "gig", "msg"}
	fmt.Println(uniqueMorseRepresentations(words))
}
