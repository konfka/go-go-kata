package main

import "fmt"

func findKthPositive(arr []int, k int) int {
	epsNumber := 1
	counter := 0
	index := 0
	incrementor := 1
	for counter != k {
		if index < len(arr) && arr[index] == incrementor {
			incrementor++
			index++
		} else {
			epsNumber = incrementor
			counter++
			incrementor++
		}
	}
	return epsNumber
}

func main() {
	arr := []int{2, 3, 4, 7, 11}
	k := 5
	fmt.Println(findKthPositive(arr, k))
}
