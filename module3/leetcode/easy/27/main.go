package main

import "fmt"

func balancedStringSplit(s string) int {
	counter := 0
	summer := 0
	for i := range s {
		if s[i] == 'L' {
			summer++
		} else {
			summer--
		}
		if summer == 0 {
			counter++
		}
	}
	return counter
}

func main() {
	s := "RLRRLLRLRL"
	fmt.Println(balancedStringSplit(s))
}
