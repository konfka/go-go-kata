package main

import "fmt"

func decompressRLElist(nums []int) []int {
	res := []int{}
	for i := 0; i < len(nums)-1; {
		multiplier := nums[i]
		number := nums[i+1]
		for j := 0; j < multiplier; j++ {
			res = append(res, number)
		}
		i += 2
	}
	return res
}

func main() {
	nums := []int{1, 2, 3, 4}
	fmt.Println(decompressRLElist(nums))
}
