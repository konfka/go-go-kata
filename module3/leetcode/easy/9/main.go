package main

import "fmt"

func finalValueAfterOperations(operations []string) int {
	x := 0
	for _, v := range operations {
		if v == "--X" || v == "X--" {
			x--
		} else {
			x++
		}
	}
	return x
}

func main() {
	operations := []string{"--X", "X++", "X++"}
	fmt.Println(finalValueAfterOperations(operations))
}
