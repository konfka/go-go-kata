package main

import "fmt"

func maximumWealth(accounts [][]int) int {
	var max, sum int
	for _, v := range accounts {
		sum = 0
		for _, val := range v {
			sum += val
		}
		if sum > max {
			max = sum
		}
	}
	return max
}

func main() {
	accounts := [][]int{{1, 2, 3}, {3, 2, 1}}
	fmt.Println(maximumWealth(accounts))
}
