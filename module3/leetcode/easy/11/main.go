package main

import "fmt"

func runningSum(nums []int) []int {
	modified := []int{nums[0]}
	for i := 1; i < len(nums); i++ {
		modified = append(modified, nums[i]+modified[i-1])
	}
	return modified
}

func main() {
	nums := []int{1, 2, 3, 4}
	fmt.Println(runningSum(nums))
}
