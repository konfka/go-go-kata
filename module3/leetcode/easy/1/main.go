package main

import "fmt"

func tribonacci(n int) int {
	sl := make([]int, 38)
	sl[0] = 0
	sl[1] = 1
	sl[2] = 1
	for i := 3; i <= n; i++ {
		sl[i] = sl[i-1] + sl[i-2] + sl[i-3]
	}
	return sl[n]
}

func main() {
	fmt.Println(tribonacci(7))
}
