package main

import "fmt"

func numberOfMatches(n int) int {
	sum := 0
	for n > 0 {
		switch {
		case n == 1:
			sum = 0
			n = 0
		case n == 2:
			sum += 1
			n = 0
		case n%2 == 0:
			sum += n / 2
			n -= n / 2
		default:
			sum += (n - 1) / 2
			n = ((n - 1) / 2) + 1
		}
	}
	return sum
}

func main() {
	fmt.Println(numberOfMatches(7))
}
