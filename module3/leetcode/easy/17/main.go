package main

import (
	"fmt"
	"strings"
)

func mostWordsFound(sentences []string) int {
	var list []string
	max := 0
	for _, v := range sentences {
		list = strings.Split(v, " ")
		if max < len(list) {
			max = len(list)
		}
	}
	return max
}

func main() {
	sentences := []string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}
	fmt.Println(mostWordsFound(sentences))
}
