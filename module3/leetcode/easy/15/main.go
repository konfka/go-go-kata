package main

type ParkingSystem struct {
	big, medium, small int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{
		big:    big,
		medium: medium,
		small:  small,
	}
}

func (ps *ParkingSystem) AddCar(carType int) bool {
	switch {
	case carType == 1 && ps.big != 0:
		ps.big -= 1
		return true
	case carType == 2 && ps.medium != 0:
		ps.medium -= 1
		return true
	case carType == 3 && ps.small != 0:
		ps.small -= 1
		return true
	default:
		return false
	}
}

func main() {
	park := Constructor(1, 1, 0)
	park.AddCar(1)
}
