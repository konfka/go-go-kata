package main

import (
	"fmt"
	"strings"
)

func interpret(command string) string {
	splitted := strings.Split(command, "")
	trans := []string{}
	for i := range splitted {
		switch {
		case splitted[i] == "G":
			trans = append(trans, "G")
		case splitted[i] == "(" && splitted[i+1] == ")":
			trans = append(trans, "o")
		case splitted[i] == "(" && splitted[i+1] == "a":
			trans = append(trans, "al")
		}
	}
	return strings.Join(trans, "")
}

func main() {
	command := "G()(al)"
	fmt.Println(interpret(command))
}
