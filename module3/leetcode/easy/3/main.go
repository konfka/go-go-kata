package main

import "fmt"

func convertTemperature(celsius float64) []float64 {
	temps := make([]float64, 2)
	temps[0] = celsius + 273.15
	temps[1] = celsius*1.8 + 32.00
	return temps
}

func main() {
	fmt.Println(convertTemperature(45))
}
