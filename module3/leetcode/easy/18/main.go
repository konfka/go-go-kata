package main

import (
	"fmt"
	"math"
	"strconv"
)

func differenceOfSum(nums []int) int {
	sumElem := 0
	sumNums := 0
	for _, v := range nums {
		sumElem += v
		numStr := strconv.Itoa(v)
		for _, digit := range numStr {
			value, _ := strconv.Atoi(string(digit))
			sumNums += value
		}
	}
	return int(math.Abs(float64(sumElem - sumNums)))
}

func main() {
	nums := []int{1, 15, 6, 3}
	fmt.Println(differenceOfSum(nums))
}
