package main

import (
	"fmt"
	"strconv"
)

func subtractProductAndSum(n int) int {
	strNum := strconv.Itoa(n)
	sum := 0
	mult := 1
	for _, v := range strNum {
		x, _ := strconv.Atoi(string(v))
		sum += x
		mult *= x
	}
	return mult - sum
}

func main() {
	fmt.Println(subtractProductAndSum(234))
}
