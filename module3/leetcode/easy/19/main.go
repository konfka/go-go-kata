package main

import (
	"fmt"
	"sort"
	"strconv"
)

func minimumSum(num int) int {
	toA := strconv.Itoa(num)
	numbers := []int{}
	for _, v := range toA {
		x, _ := strconv.Atoi(string(v))
		numbers = append(numbers, x)
	}
	sort.Ints(numbers)
	f, _ := strconv.Atoi(strconv.Itoa(numbers[0]) + strconv.Itoa(numbers[2]))
	s, _ := strconv.Atoi(strconv.Itoa(numbers[1]) + strconv.Itoa(numbers[3]))
	return f + s
}

func main() {
	fmt.Println(minimumSum(2932))
}
