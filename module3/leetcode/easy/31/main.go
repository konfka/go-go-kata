package main

import (
	"fmt"
	"sort"
)

func sortPeople(names []string, heights []int) []string {
	people := make(map[int]string)
	sortedHeights := []int{}
	for i := range names {
		people[heights[i]] = names[i]
		sortedHeights = append(sortedHeights, heights[i])
	}
	sort.Ints(sortedHeights)
	res := []string{}
	for i := len(names) - 1; i != -1; i-- {
		res = append(res, people[sortedHeights[i]])
	}
	return res
}

func main() {
	names := []string{"Mary", "John", "Emma"}
	heights := []int{180, 165, 170}
	fmt.Println(sortPeople(names, heights))
}
