package main

import (
	"fmt"
	"strings"
)

func numJewelsInStones(jewels string, stones string) int {
	counter := 0
	jews := strings.Split(jewels, "")
	stns := strings.Split(stones, "")
	for _, val1 := range jews {
		for _, val2 := range stns {
			if val1 == val2 {
				counter++
			}
		}
	}
	return counter
}

func main() {
	jewels := "aA"
	stones := "aAAbbbb"
	fmt.Println(numJewelsInStones(jewels, stones))
}
