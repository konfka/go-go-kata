package main

import "fmt"

func numIdenticalPairs(nums []int) int {
	counter := 0
	for i := range nums {
		for j := range nums {
			if i < j && nums[i] == nums[j] {
				counter++
			}
		}
	}
	return counter
}

func main() {
	nums := []int{1, 2, 3, 1, 1, 3}
	fmt.Println(numIdenticalPairs(nums))
}
