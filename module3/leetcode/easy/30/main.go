package main

import (
	"fmt"
	"math"
)

func countGoodTriplets(arr []int, a int, b int, c int) int {
	count := 0
	for i := range arr {
		for j := range arr {
			for k := range arr {
				if i < j && j < k && k < len(arr) && int(math.Abs(float64(arr[i]-arr[j]))) <= a && int(math.Abs(float64(arr[j]-arr[k]))) <= b && int(math.Abs(float64(arr[i]-arr[k]))) <= c {
					count++
				}
			}
		}
	}
	return count
}

func main() {
	arr := []int{3, 0, 1, 1, 9, 7}
	a := 7
	b := 2
	c := 3
	fmt.Println(countGoodTriplets(arr, a, b, c))
}
