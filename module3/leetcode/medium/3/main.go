package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	res := &ListNode{}
	current := head
	for current.Next != nil {
		if current.Val != 0 {
			res.Val += current.Val
			*current = *current.Next
		} else {
			res = current
			current = current.Next
		}
	}
	res.Next = nil
	return head
}

func (l *ListNode) Insert(nums []int) {
	for _, v := range nums {
		newNode := &ListNode{Val: v}

		current := l
		current.Val = v
		for current.Next != nil {
			current = current.Next
		}
		current.Next = newNode
	}
}

func Show(l *ListNode) {
	p := l
	for p.Next != nil {
		fmt.Println(p.Val)
		p = p.Next
	}
}

func main() {
	list := &ListNode{}
	head := []int{0, 1, 0, 3, 0, 2, 2, 0}
	list.Insert(head)
	Show(list)
	merged := mergeNodes(list)
	Show(merged)
}
