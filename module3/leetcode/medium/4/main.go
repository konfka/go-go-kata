package main

import (
	"errors"
	"fmt"
)

type TreeNode struct {
	Left  *TreeNode
	Right *TreeNode
	Val   int
}

func search(root *TreeNode, sum *int) {

	if root == nil {
		return
	}
	search(root.Right, sum)
	*sum += root.Val
	root.Val = *sum
	search(root.Left, sum)

}

func bstToGst(root *TreeNode) *TreeNode {
	if root == nil {
		_ = errors.New("Tree is empty")
		return nil
	}
	sum := 0
	search(root, &sum)
	return root
}

func main() {
	tree := &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val: 2,
			Left: &TreeNode{
				Val: 4,
			},
		},
		Right: &TreeNode{
			Val: 3,
			Left: &TreeNode{
				Val: 5,
				Left: &TreeNode{
					Val: 7,
				},
				Right: &TreeNode{
					Val: 8,
				},
			},
			Right: &TreeNode{Val: 6},
		},
	}
	fmt.Println(bstToGst(tree))
}
