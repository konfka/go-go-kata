package main

import (
	"errors"
	"fmt"
	"os"
	"time"
)

type Post struct {
	body        string
	publishDate int64
	next        *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}
	f.length++
}

func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic(errors.New("Feed is empty"))
	}
	var previousPost *Post
	currentPost := f.start
	for currentPost.publishDate != publishDate {
		if currentPost.next == nil {
			panic(errors.New("No such post found."))
		}
		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next
	f.length--
}

func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = currentPost.next
		}
		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

func (f Feed) Inspect() {
	fmt.Println("=======================================")
	fmt.Printf("Feed length: %d\n", f.length)
	currentPost := f.start
	for i := 0; i < f.length; i++ {
		if f.end == nil {
			fmt.Println("=======================================")
			os.Exit(0)
		}
		fmt.Printf("Item: %d - %v\n", i, currentPost)
		currentPost = currentPost.next
	}
	fmt.Println("=======================================")
}

func main() {
	rightNow := time.Now().Unix()
	posts := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	posts.Append(p1)
	posts.Append(p2)
	posts.Append(p3)
	posts.Append(p4)

	posts.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	posts.Insert(newPost)
	posts.Inspect()
	posts.Remove(p4.publishDate)
	posts.Inspect()
}
